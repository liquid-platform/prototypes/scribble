// Generated from /home/niklas/Projects/personal/scribble/grammar/Scribble.g4 by ANTLR 4.8
package antlr4;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ScribbleParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, MUTABLE_VAR_DEF=29, IMMUTABLE_VAR_DEF=30, 
		DIGIT=31, STRING=32, ESC=33, IDENTIFIER=34, OPERATOR=35, WS=36, NL=37, 
		SYM_SCOPE_OPEN=38, SYM_SCOPE_CLOSE=39, LINE_COMMENT=40, MULTILINE_COMMENT=41;
	public static final int
		RULE_scribble = 0, RULE_moduleDeclaration = 1, RULE_importDeclaration = 2, 
		RULE_recordDefinition = 3, RULE_recordMemberDefinition = 4, RULE_functionDefinition = 5, 
		RULE_functionParameter = 6, RULE_statement = 7, RULE_loop = 8, RULE_variableDefinition = 9, 
		RULE_variableAssignment = 10, RULE_expression = 11, RULE_conditional = 12, 
		RULE_recordMemberSelection = 13, RULE_symbolReference = 14, RULE_constructorInvocation = 15, 
		RULE_blockOfCode = 16, RULE_mapLiteral = 17, RULE_listLiteral = 18, RULE_numberLiteral = 19, 
		RULE_stringLiteral = 20, RULE_fullyQualifiedModuleName = 21, RULE_functionIdentifier = 22;
	private static String[] makeRuleNames() {
		return new String[] {
			"scribble", "moduleDeclaration", "importDeclaration", "recordDefinition", 
			"recordMemberDefinition", "functionDefinition", "functionParameter", 
			"statement", "loop", "variableDefinition", "variableAssignment", "expression", 
			"conditional", "recordMemberSelection", "symbolReference", "constructorInvocation", 
			"blockOfCode", "mapLiteral", "listLiteral", "numberLiteral", "stringLiteral", 
			"fullyQualifiedModuleName", "functionIdentifier"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'module'", "'import'", "'record'", "':'", "','", "'fun'", "'('", 
			"')'", "'->'", "'foreach'", "'_'", "'in'", "'='", "'^'", "'*'", "'/'", 
			"'+'", "'-'", "'if'", "'else if'", "'else'", "'.'", "'(:)'", "'['", "']'", 
			"'0b'", "'0x'", "'0o'", "'var'", "'let'", null, null, null, null, null, 
			null, null, "'{'", "'}'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, "MUTABLE_VAR_DEF", "IMMUTABLE_VAR_DEF", 
			"DIGIT", "STRING", "ESC", "IDENTIFIER", "OPERATOR", "WS", "NL", "SYM_SCOPE_OPEN", 
			"SYM_SCOPE_CLOSE", "LINE_COMMENT", "MULTILINE_COMMENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Scribble.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ScribbleParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ScribbleContext extends ParserRuleContext {
		public List<TerminalNode> NL() { return getTokens(ScribbleParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(ScribbleParser.NL, i);
		}
		public ModuleDeclarationContext moduleDeclaration() {
			return getRuleContext(ModuleDeclarationContext.class,0);
		}
		public ImportDeclarationContext importDeclaration() {
			return getRuleContext(ImportDeclarationContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public ScribbleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_scribble; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterScribble(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitScribble(this);
		}
	}

	public final ScribbleContext scribble() throws RecognitionException {
		ScribbleContext _localctx = new ScribbleContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_scribble);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(49);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(46);
					match(NL);
					}
					} 
				}
				setState(51);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			setState(59);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__0) {
				{
				setState(52);
				moduleDeclaration();
				setState(56);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(53);
						match(NL);
						}
						} 
					}
					setState(58);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
				}
				}
			}

			setState(68);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__1) {
				{
				setState(61);
				importDeclaration();
				setState(65);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(62);
						match(NL);
						}
						} 
					}
					setState(67);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
				}
				}
			}

			setState(82);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__5) | (1L << T__9) | (1L << T__18) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << MUTABLE_VAR_DEF) | (1L << IMMUTABLE_VAR_DEF) | (1L << DIGIT) | (1L << STRING) | (1L << IDENTIFIER) | (1L << OPERATOR) | (1L << SYM_SCOPE_OPEN))) != 0)) {
				{
				setState(70);
				statement();
				setState(79);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(72); 
						_errHandler.sync(this);
						_la = _input.LA(1);
						do {
							{
							{
							setState(71);
							match(NL);
							}
							}
							setState(74); 
							_errHandler.sync(this);
							_la = _input.LA(1);
						} while ( _la==NL );
						setState(76);
						statement();
						}
						} 
					}
					setState(81);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
				}
				}
			}

			setState(87);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(84);
				match(NL);
				}
				}
				setState(89);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModuleDeclarationContext extends ParserRuleContext {
		public FullyQualifiedModuleNameContext fullyQualifiedModuleName() {
			return getRuleContext(FullyQualifiedModuleNameContext.class,0);
		}
		public TerminalNode SYM_SCOPE_OPEN() { return getToken(ScribbleParser.SYM_SCOPE_OPEN, 0); }
		public TerminalNode SYM_SCOPE_CLOSE() { return getToken(ScribbleParser.SYM_SCOPE_CLOSE, 0); }
		public List<TerminalNode> NL() { return getTokens(ScribbleParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(ScribbleParser.NL, i);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(ScribbleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ScribbleParser.IDENTIFIER, i);
		}
		public ModuleDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_moduleDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterModuleDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitModuleDeclaration(this);
		}
	}

	public final ModuleDeclarationContext moduleDeclaration() throws RecognitionException {
		ModuleDeclarationContext _localctx = new ModuleDeclarationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_moduleDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(90);
			match(T__0);
			setState(91);
			fullyQualifiedModuleName();
			setState(92);
			match(SYM_SCOPE_OPEN);
			setState(96);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(93);
				match(NL);
				}
				}
				setState(98);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(103);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==IDENTIFIER) {
				{
				{
				setState(99);
				match(IDENTIFIER);
				setState(100);
				match(NL);
				}
				}
				setState(105);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(106);
			match(SYM_SCOPE_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImportDeclarationContext extends ParserRuleContext {
		public TerminalNode SYM_SCOPE_OPEN() { return getToken(ScribbleParser.SYM_SCOPE_OPEN, 0); }
		public TerminalNode SYM_SCOPE_CLOSE() { return getToken(ScribbleParser.SYM_SCOPE_CLOSE, 0); }
		public List<TerminalNode> NL() { return getTokens(ScribbleParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(ScribbleParser.NL, i);
		}
		public List<FullyQualifiedModuleNameContext> fullyQualifiedModuleName() {
			return getRuleContexts(FullyQualifiedModuleNameContext.class);
		}
		public FullyQualifiedModuleNameContext fullyQualifiedModuleName(int i) {
			return getRuleContext(FullyQualifiedModuleNameContext.class,i);
		}
		public ImportDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_importDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterImportDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitImportDeclaration(this);
		}
	}

	public final ImportDeclarationContext importDeclaration() throws RecognitionException {
		ImportDeclarationContext _localctx = new ImportDeclarationContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_importDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(108);
			match(T__1);
			setState(109);
			match(SYM_SCOPE_OPEN);
			setState(113);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(110);
				match(NL);
				}
				}
				setState(115);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(122);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==IDENTIFIER) {
				{
				{
				setState(116);
				fullyQualifiedModuleName();
				setState(118);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NL) {
					{
					setState(117);
					match(NL);
					}
				}

				}
				}
				setState(124);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(125);
			match(SYM_SCOPE_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RecordDefinitionContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(ScribbleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ScribbleParser.IDENTIFIER, i);
		}
		public TerminalNode SYM_SCOPE_OPEN() { return getToken(ScribbleParser.SYM_SCOPE_OPEN, 0); }
		public TerminalNode SYM_SCOPE_CLOSE() { return getToken(ScribbleParser.SYM_SCOPE_CLOSE, 0); }
		public List<TerminalNode> NL() { return getTokens(ScribbleParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(ScribbleParser.NL, i);
		}
		public List<RecordMemberDefinitionContext> recordMemberDefinition() {
			return getRuleContexts(RecordMemberDefinitionContext.class);
		}
		public RecordMemberDefinitionContext recordMemberDefinition(int i) {
			return getRuleContext(RecordMemberDefinitionContext.class,i);
		}
		public RecordDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_recordDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterRecordDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitRecordDefinition(this);
		}
	}

	public final RecordDefinitionContext recordDefinition() throws RecognitionException {
		RecordDefinitionContext _localctx = new RecordDefinitionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_recordDefinition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(127);
			match(T__2);
			setState(128);
			match(IDENTIFIER);
			setState(138);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__3) {
				{
				setState(129);
				match(T__3);
				setState(130);
				match(IDENTIFIER);
				setState(135);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__4) {
					{
					{
					setState(131);
					match(T__4);
					setState(132);
					match(IDENTIFIER);
					}
					}
					setState(137);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(160);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SYM_SCOPE_OPEN) {
				{
				setState(140);
				match(SYM_SCOPE_OPEN);
				setState(144);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(141);
					match(NL);
					}
					}
					setState(146);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(156);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==IDENTIFIER) {
					{
					{
					setState(147);
					recordMemberDefinition();
					setState(151);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(148);
						match(NL);
						}
						}
						setState(153);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					}
					setState(158);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(159);
				match(SYM_SCOPE_CLOSE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RecordMemberDefinitionContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(ScribbleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ScribbleParser.IDENTIFIER, i);
		}
		public RecordMemberDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_recordMemberDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterRecordMemberDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitRecordMemberDefinition(this);
		}
	}

	public final RecordMemberDefinitionContext recordMemberDefinition() throws RecognitionException {
		RecordMemberDefinitionContext _localctx = new RecordMemberDefinitionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_recordMemberDefinition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(162);
			match(IDENTIFIER);
			setState(163);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDefinitionContext extends ParserRuleContext {
		public BlockOfCodeContext blockOfCode() {
			return getRuleContext(BlockOfCodeContext.class,0);
		}
		public FunctionIdentifierContext functionIdentifier() {
			return getRuleContext(FunctionIdentifierContext.class,0);
		}
		public List<FunctionParameterContext> functionParameter() {
			return getRuleContexts(FunctionParameterContext.class);
		}
		public FunctionParameterContext functionParameter(int i) {
			return getRuleContext(FunctionParameterContext.class,i);
		}
		public TerminalNode IDENTIFIER() { return getToken(ScribbleParser.IDENTIFIER, 0); }
		public FunctionDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterFunctionDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitFunctionDefinition(this);
		}
	}

	public final FunctionDefinitionContext functionDefinition() throws RecognitionException {
		FunctionDefinitionContext _localctx = new FunctionDefinitionContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_functionDefinition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(165);
			match(T__5);
			{
			setState(166);
			functionIdentifier();
			}
			setState(178);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__6) {
				{
				setState(167);
				match(T__6);
				setState(168);
				functionParameter();
				setState(173);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__4) {
					{
					{
					setState(169);
					match(T__4);
					setState(170);
					functionParameter();
					}
					}
					setState(175);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(176);
				match(T__7);
				}
			}

			setState(182);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__8) {
				{
				setState(180);
				match(T__8);
				setState(181);
				match(IDENTIFIER);
				}
			}

			setState(184);
			blockOfCode();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionParameterContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(ScribbleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ScribbleParser.IDENTIFIER, i);
		}
		public FunctionParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionParameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterFunctionParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitFunctionParameter(this);
		}
	}

	public final FunctionParameterContext functionParameter() throws RecognitionException {
		FunctionParameterContext _localctx = new FunctionParameterContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_functionParameter);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(186);
			match(IDENTIFIER);
			setState(188);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(187);
				match(IDENTIFIER);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public RecordDefinitionContext recordDefinition() {
			return getRuleContext(RecordDefinitionContext.class,0);
		}
		public FunctionDefinitionContext functionDefinition() {
			return getRuleContext(FunctionDefinitionContext.class,0);
		}
		public LoopContext loop() {
			return getRuleContext(LoopContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VariableDefinitionContext variableDefinition() {
			return getRuleContext(VariableDefinitionContext.class,0);
		}
		public VariableAssignmentContext variableAssignment() {
			return getRuleContext(VariableAssignmentContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(196);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				{
				setState(190);
				recordDefinition();
				}
				break;
			case 2:
				{
				setState(191);
				functionDefinition();
				}
				break;
			case 3:
				{
				setState(192);
				loop();
				}
				break;
			case 4:
				{
				setState(193);
				expression(0);
				}
				break;
			case 5:
				{
				setState(194);
				variableDefinition();
				}
				break;
			case 6:
				{
				setState(195);
				variableAssignment();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LoopContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BlockOfCodeContext blockOfCode() {
			return getRuleContext(BlockOfCodeContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(ScribbleParser.IDENTIFIER, 0); }
		public LoopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_loop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterLoop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitLoop(this);
		}
	}

	public final LoopContext loop() throws RecognitionException {
		LoopContext _localctx = new LoopContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_loop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(198);
			match(T__9);
			setState(199);
			_la = _input.LA(1);
			if ( !(_la==T__10 || _la==IDENTIFIER) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(200);
			match(T__11);
			setState(201);
			expression(0);
			setState(202);
			blockOfCode();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDefinitionContext extends ParserRuleContext {
		public Token key;
		public List<TerminalNode> IDENTIFIER() { return getTokens(ScribbleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ScribbleParser.IDENTIFIER, i);
		}
		public TerminalNode MUTABLE_VAR_DEF() { return getToken(ScribbleParser.MUTABLE_VAR_DEF, 0); }
		public TerminalNode IMMUTABLE_VAR_DEF() { return getToken(ScribbleParser.IMMUTABLE_VAR_DEF, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VariableDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterVariableDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitVariableDefinition(this);
		}
	}

	public final VariableDefinitionContext variableDefinition() throws RecognitionException {
		VariableDefinitionContext _localctx = new VariableDefinitionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_variableDefinition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(204);
			((VariableDefinitionContext)_localctx).key = _input.LT(1);
			_la = _input.LA(1);
			if ( !(_la==MUTABLE_VAR_DEF || _la==IMMUTABLE_VAR_DEF) ) {
				((VariableDefinitionContext)_localctx).key = (Token)_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(205);
			match(IDENTIFIER);
			setState(207);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(206);
				match(IDENTIFIER);
				}
			}

			setState(211);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__12) {
				{
				setState(209);
				match(T__12);
				setState(210);
				expression(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableAssignmentContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ScribbleParser.IDENTIFIER, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode OPERATOR() { return getToken(ScribbleParser.OPERATOR, 0); }
		public VariableAssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableAssignment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterVariableAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitVariableAssignment(this);
		}
	}

	public final VariableAssignmentContext variableAssignment() throws RecognitionException {
		VariableAssignmentContext _localctx = new VariableAssignmentContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_variableAssignment);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(213);
			match(IDENTIFIER);
			setState(215);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==OPERATOR) {
				{
				setState(214);
				match(OPERATOR);
				}
			}

			setState(217);
			match(T__12);
			setState(218);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ConstructorInvocationEContext extends ExpressionContext {
		public ConstructorInvocationContext constructorInvocation() {
			return getRuleContext(ConstructorInvocationContext.class,0);
		}
		public ConstructorInvocationEContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterConstructorInvocationE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitConstructorInvocationE(this);
		}
	}
	public static class StringLiteralEContext extends ExpressionContext {
		public StringLiteralContext stringLiteral() {
			return getRuleContext(StringLiteralContext.class,0);
		}
		public StringLiteralEContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterStringLiteralE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitStringLiteralE(this);
		}
	}
	public static class ConditionalEContext extends ExpressionContext {
		public ConditionalContext conditional() {
			return getRuleContext(ConditionalContext.class,0);
		}
		public ConditionalEContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterConditionalE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitConditionalE(this);
		}
	}
	public static class StdInfixExpressionContext extends ExpressionContext {
		public Token op;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public StdInfixExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterStdInfixExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitStdInfixExpression(this);
		}
	}
	public static class PrefixExpressionContext extends ExpressionContext {
		public TerminalNode OPERATOR() { return getToken(ScribbleParser.OPERATOR, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public PrefixExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterPrefixExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitPrefixExpression(this);
		}
	}
	public static class RecordMemberSelectionEContext extends ExpressionContext {
		public RecordMemberSelectionContext recordMemberSelection() {
			return getRuleContext(RecordMemberSelectionContext.class,0);
		}
		public RecordMemberSelectionEContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterRecordMemberSelectionE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitRecordMemberSelectionE(this);
		}
	}
	public static class BlockOfCodeEContext extends ExpressionContext {
		public BlockOfCodeContext blockOfCode() {
			return getRuleContext(BlockOfCodeContext.class,0);
		}
		public BlockOfCodeEContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterBlockOfCodeE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitBlockOfCodeE(this);
		}
	}
	public static class InfixExpressionContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public FunctionIdentifierContext functionIdentifier() {
			return getRuleContext(FunctionIdentifierContext.class,0);
		}
		public InfixExpressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterInfixExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitInfixExpression(this);
		}
	}
	public static class NumberLiteralEContext extends ExpressionContext {
		public NumberLiteralContext numberLiteral() {
			return getRuleContext(NumberLiteralContext.class,0);
		}
		public NumberLiteralEContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterNumberLiteralE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitNumberLiteralE(this);
		}
	}
	public static class SymbolReferenceEContext extends ExpressionContext {
		public SymbolReferenceContext symbolReference() {
			return getRuleContext(SymbolReferenceContext.class,0);
		}
		public SymbolReferenceEContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterSymbolReferenceE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitSymbolReferenceE(this);
		}
	}
	public static class ListLiteralEContext extends ExpressionContext {
		public ListLiteralContext listLiteral() {
			return getRuleContext(ListLiteralContext.class,0);
		}
		public ListLiteralEContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterListLiteralE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitListLiteralE(this);
		}
	}
	public static class MapLiteralEContext extends ExpressionContext {
		public MapLiteralContext mapLiteral() {
			return getRuleContext(MapLiteralContext.class,0);
		}
		public MapLiteralEContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterMapLiteralE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitMapLiteralE(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 22;
		enterRecursionRule(_localctx, 22, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(232);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
			case 1:
				{
				_localctx = new PrefixExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(221);
				match(OPERATOR);
				setState(222);
				expression(14);
				}
				break;
			case 2:
				{
				_localctx = new ConditionalEContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(223);
				conditional();
				}
				break;
			case 3:
				{
				_localctx = new RecordMemberSelectionEContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(224);
				recordMemberSelection();
				}
				break;
			case 4:
				{
				_localctx = new SymbolReferenceEContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(225);
				symbolReference();
				}
				break;
			case 5:
				{
				_localctx = new ConstructorInvocationEContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(226);
				constructorInvocation();
				}
				break;
			case 6:
				{
				_localctx = new StringLiteralEContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(227);
				stringLiteral();
				}
				break;
			case 7:
				{
				_localctx = new NumberLiteralEContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(228);
				numberLiteral();
				}
				break;
			case 8:
				{
				_localctx = new MapLiteralEContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(229);
				mapLiteral();
				}
				break;
			case 9:
				{
				_localctx = new ListLiteralEContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(230);
				listLiteral();
				}
				break;
			case 10:
				{
				_localctx = new BlockOfCodeEContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(231);
				blockOfCode();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(249);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(247);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,29,_ctx) ) {
					case 1:
						{
						_localctx = new InfixExpressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(234);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(235);
						functionIdentifier();
						setState(236);
						expression(14);
						}
						break;
					case 2:
						{
						_localctx = new StdInfixExpressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(238);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(239);
						((StdInfixExpressionContext)_localctx).op = match(T__13);
						setState(240);
						expression(13);
						}
						break;
					case 3:
						{
						_localctx = new StdInfixExpressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(241);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(242);
						((StdInfixExpressionContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__14 || _la==T__15) ) {
							((StdInfixExpressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(243);
						expression(12);
						}
						break;
					case 4:
						{
						_localctx = new StdInfixExpressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(244);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(245);
						((StdInfixExpressionContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__16 || _la==T__17) ) {
							((StdInfixExpressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(246);
						expression(11);
						}
						break;
					}
					} 
				}
				setState(251);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ConditionalContext extends ParserRuleContext {
		public ExpressionContext ifCondition;
		public BlockOfCodeContext ifBranchBody;
		public ExpressionContext elseIfCondition;
		public BlockOfCodeContext elseIfBranchBody;
		public BlockOfCodeContext elseBranchBody;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<BlockOfCodeContext> blockOfCode() {
			return getRuleContexts(BlockOfCodeContext.class);
		}
		public BlockOfCodeContext blockOfCode(int i) {
			return getRuleContext(BlockOfCodeContext.class,i);
		}
		public ConditionalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditional; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterConditional(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitConditional(this);
		}
	}

	public final ConditionalContext conditional() throws RecognitionException {
		ConditionalContext _localctx = new ConditionalContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_conditional);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(252);
			match(T__18);
			setState(253);
			((ConditionalContext)_localctx).ifCondition = expression(0);
			setState(254);
			((ConditionalContext)_localctx).ifBranchBody = blockOfCode();
			setState(261);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,31,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(255);
					match(T__19);
					setState(256);
					((ConditionalContext)_localctx).elseIfCondition = expression(0);
					setState(257);
					((ConditionalContext)_localctx).elseIfBranchBody = blockOfCode();
					}
					} 
				}
				setState(263);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,31,_ctx);
			}
			setState(266);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
			case 1:
				{
				setState(264);
				match(T__20);
				setState(265);
				((ConditionalContext)_localctx).elseBranchBody = blockOfCode();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RecordMemberSelectionContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(ScribbleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ScribbleParser.IDENTIFIER, i);
		}
		public RecordMemberSelectionContext recordMemberSelection() {
			return getRuleContext(RecordMemberSelectionContext.class,0);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public RecordMemberSelectionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_recordMemberSelection; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterRecordMemberSelection(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitRecordMemberSelection(this);
		}
	}

	public final RecordMemberSelectionContext recordMemberSelection() throws RecognitionException {
		RecordMemberSelectionContext _localctx = new RecordMemberSelectionContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_recordMemberSelection);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(268);
			match(IDENTIFIER);
			setState(269);
			match(T__21);
			setState(272);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
			case 1:
				{
				setState(270);
				match(IDENTIFIER);
				}
				break;
			case 2:
				{
				setState(271);
				recordMemberSelection();
				}
				break;
			}
			setState(285);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
			case 1:
				{
				setState(274);
				match(T__6);
				setState(275);
				expression(0);
				setState(280);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__4) {
					{
					{
					setState(276);
					match(T__4);
					setState(277);
					expression(0);
					}
					}
					setState(282);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(283);
				match(T__7);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SymbolReferenceContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ScribbleParser.IDENTIFIER, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> NL() { return getTokens(ScribbleParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(ScribbleParser.NL, i);
		}
		public SymbolReferenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_symbolReference; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterSymbolReference(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitSymbolReference(this);
		}
	}

	public final SymbolReferenceContext symbolReference() throws RecognitionException {
		SymbolReferenceContext _localctx = new SymbolReferenceContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_symbolReference);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(287);
			match(IDENTIFIER);
			setState(323);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,41,_ctx) ) {
			case 1:
				{
				setState(288);
				match(T__6);
				setState(292);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(289);
					match(NL);
					}
					}
					setState(294);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(295);
				expression(0);
				setState(299);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==NL) {
					{
					{
					setState(296);
					match(NL);
					}
					}
					setState(301);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(318);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__4) {
					{
					{
					setState(302);
					match(T__4);
					setState(306);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(303);
						match(NL);
						}
						}
						setState(308);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(309);
					expression(0);
					setState(313);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(310);
						match(NL);
						}
						}
						setState(315);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					}
					setState(320);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(321);
				match(T__7);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructorInvocationContext extends ParserRuleContext {
		public Token recordIdentifier;
		public Token fieldIdentifier;
		public TerminalNode SYM_SCOPE_OPEN() { return getToken(ScribbleParser.SYM_SCOPE_OPEN, 0); }
		public TerminalNode SYM_SCOPE_CLOSE() { return getToken(ScribbleParser.SYM_SCOPE_CLOSE, 0); }
		public List<TerminalNode> IDENTIFIER() { return getTokens(ScribbleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ScribbleParser.IDENTIFIER, i);
		}
		public List<TerminalNode> NL() { return getTokens(ScribbleParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(ScribbleParser.NL, i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ConstructorInvocationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constructorInvocation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterConstructorInvocation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitConstructorInvocation(this);
		}
	}

	public final ConstructorInvocationContext constructorInvocation() throws RecognitionException {
		ConstructorInvocationContext _localctx = new ConstructorInvocationContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_constructorInvocation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(325);
			((ConstructorInvocationContext)_localctx).recordIdentifier = match(IDENTIFIER);
			setState(326);
			match(SYM_SCOPE_OPEN);
			setState(328); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(327);
				match(NL);
				}
				}
				setState(330); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==NL );
			setState(342);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==IDENTIFIER) {
				{
				{
				setState(332);
				((ConstructorInvocationContext)_localctx).fieldIdentifier = match(IDENTIFIER);
				setState(333);
				match(T__12);
				setState(334);
				expression(0);
				setState(336); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(335);
					match(NL);
					}
					}
					setState(338); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==NL );
				}
				}
				setState(344);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(345);
			match(SYM_SCOPE_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockOfCodeContext extends ParserRuleContext {
		public TerminalNode SYM_SCOPE_OPEN() { return getToken(ScribbleParser.SYM_SCOPE_OPEN, 0); }
		public TerminalNode SYM_SCOPE_CLOSE() { return getToken(ScribbleParser.SYM_SCOPE_CLOSE, 0); }
		public List<TerminalNode> NL() { return getTokens(ScribbleParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(ScribbleParser.NL, i);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public BlockOfCodeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blockOfCode; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterBlockOfCode(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitBlockOfCode(this);
		}
	}

	public final BlockOfCodeContext blockOfCode() throws RecognitionException {
		BlockOfCodeContext _localctx = new BlockOfCodeContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_blockOfCode);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(347);
			match(SYM_SCOPE_OPEN);
			setState(351);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(348);
				match(NL);
				}
				}
				setState(353);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(362);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__5) | (1L << T__9) | (1L << T__18) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << MUTABLE_VAR_DEF) | (1L << IMMUTABLE_VAR_DEF) | (1L << DIGIT) | (1L << STRING) | (1L << IDENTIFIER) | (1L << OPERATOR) | (1L << SYM_SCOPE_OPEN))) != 0)) {
				{
				{
				setState(354);
				statement();
				setState(356); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(355);
					match(NL);
					}
					}
					setState(358); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==NL );
				}
				}
				setState(364);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(365);
			match(SYM_SCOPE_CLOSE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MapLiteralContext extends ParserRuleContext {
		public MapLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mapLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterMapLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitMapLiteral(this);
		}
	}

	public final MapLiteralContext mapLiteral() throws RecognitionException {
		MapLiteralContext _localctx = new MapLiteralContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_mapLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(367);
			match(T__22);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ListLiteralContext extends ParserRuleContext {
		public List<TerminalNode> NL() { return getTokens(ScribbleParser.NL); }
		public TerminalNode NL(int i) {
			return getToken(ScribbleParser.NL, i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ListLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_listLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterListLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitListLiteral(this);
		}
	}

	public final ListLiteralContext listLiteral() throws RecognitionException {
		ListLiteralContext _localctx = new ListLiteralContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_listLiteral);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(369);
			match(T__23);
			setState(373);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,48,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(370);
					match(NL);
					}
					} 
				}
				setState(375);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,48,_ctx);
			}
			setState(396);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__18) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << DIGIT) | (1L << STRING) | (1L << IDENTIFIER) | (1L << OPERATOR) | (1L << SYM_SCOPE_OPEN))) != 0)) {
				{
				setState(376);
				expression(0);
				setState(380);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,49,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(377);
						match(NL);
						}
						} 
					}
					setState(382);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,49,_ctx);
				}
				setState(393);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__4) {
					{
					{
					setState(383);
					match(T__4);
					setState(387);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==NL) {
						{
						{
						setState(384);
						match(NL);
						}
						}
						setState(389);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(390);
					expression(0);
					}
					}
					setState(395);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(401);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NL) {
				{
				{
				setState(398);
				match(NL);
				}
				}
				setState(403);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(404);
			match(T__24);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberLiteralContext extends ParserRuleContext {
		public NumberLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numberLiteral; }
	 
		public NumberLiteralContext() { }
		public void copyFrom(NumberLiteralContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class DecimalFloatingPointContext extends NumberLiteralContext {
		public Token beforeComma;
		public Token afterComma;
		public List<TerminalNode> DIGIT() { return getTokens(ScribbleParser.DIGIT); }
		public TerminalNode DIGIT(int i) {
			return getToken(ScribbleParser.DIGIT, i);
		}
		public DecimalFloatingPointContext(NumberLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterDecimalFloatingPoint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitDecimalFloatingPoint(this);
		}
	}
	public static class BinaryNumberLiteralContext extends NumberLiteralContext {
		public List<TerminalNode> DIGIT() { return getTokens(ScribbleParser.DIGIT); }
		public TerminalNode DIGIT(int i) {
			return getToken(ScribbleParser.DIGIT, i);
		}
		public BinaryNumberLiteralContext(NumberLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterBinaryNumberLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitBinaryNumberLiteral(this);
		}
	}
	public static class OctNumberLiteralContext extends NumberLiteralContext {
		public List<TerminalNode> DIGIT() { return getTokens(ScribbleParser.DIGIT); }
		public TerminalNode DIGIT(int i) {
			return getToken(ScribbleParser.DIGIT, i);
		}
		public OctNumberLiteralContext(NumberLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterOctNumberLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitOctNumberLiteral(this);
		}
	}
	public static class DecimalIntegerContext extends NumberLiteralContext {
		public List<TerminalNode> DIGIT() { return getTokens(ScribbleParser.DIGIT); }
		public TerminalNode DIGIT(int i) {
			return getToken(ScribbleParser.DIGIT, i);
		}
		public DecimalIntegerContext(NumberLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterDecimalInteger(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitDecimalInteger(this);
		}
	}
	public static class HexNumberLiteralContext extends NumberLiteralContext {
		public List<TerminalNode> DIGIT() { return getTokens(ScribbleParser.DIGIT); }
		public TerminalNode DIGIT(int i) {
			return getToken(ScribbleParser.DIGIT, i);
		}
		public HexNumberLiteralContext(NumberLiteralContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterHexNumberLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitHexNumberLiteral(this);
		}
	}

	public final NumberLiteralContext numberLiteral() throws RecognitionException {
		NumberLiteralContext _localctx = new NumberLiteralContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_numberLiteral);
		int _la;
		try {
			int _alt;
			setState(441);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,60,_ctx) ) {
			case 1:
				_localctx = new BinaryNumberLiteralContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(406);
				match(T__25);
				setState(408); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(407);
						match(DIGIT);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(410); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,54,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				}
				break;
			case 2:
				_localctx = new HexNumberLiteralContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(412);
				match(T__26);
				setState(414); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(413);
						match(DIGIT);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(416); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,55,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				}
				break;
			case 3:
				_localctx = new OctNumberLiteralContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(418);
				match(T__27);
				setState(420); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(419);
						match(DIGIT);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(422); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,56,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				}
				break;
			case 4:
				_localctx = new DecimalIntegerContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(425); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(424);
						match(DIGIT);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(427); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,57,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				}
				break;
			case 5:
				_localctx = new DecimalFloatingPointContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(432);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==DIGIT) {
					{
					{
					setState(429);
					((DecimalFloatingPointContext)_localctx).beforeComma = match(DIGIT);
					}
					}
					setState(434);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(435);
				match(T__21);
				setState(437); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(436);
						((DecimalFloatingPointContext)_localctx).afterComma = match(DIGIT);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(439); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,59,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringLiteralContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(ScribbleParser.STRING, 0); }
		public StringLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterStringLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitStringLiteral(this);
		}
	}

	public final StringLiteralContext stringLiteral() throws RecognitionException {
		StringLiteralContext _localctx = new StringLiteralContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_stringLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(443);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FullyQualifiedModuleNameContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(ScribbleParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ScribbleParser.IDENTIFIER, i);
		}
		public FullyQualifiedModuleNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fullyQualifiedModuleName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterFullyQualifiedModuleName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitFullyQualifiedModuleName(this);
		}
	}

	public final FullyQualifiedModuleNameContext fullyQualifiedModuleName() throws RecognitionException {
		FullyQualifiedModuleNameContext _localctx = new FullyQualifiedModuleNameContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_fullyQualifiedModuleName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(445);
			match(IDENTIFIER);
			setState(450);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__21) {
				{
				{
				setState(446);
				match(T__21);
				setState(447);
				match(IDENTIFIER);
				}
				}
				setState(452);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionIdentifierContext extends ParserRuleContext {
		public FunctionIdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionIdentifier; }
	 
		public FunctionIdentifierContext() { }
		public void copyFrom(FunctionIdentifierContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AlphanumericFunctionIdentifierContext extends FunctionIdentifierContext {
		public TerminalNode IDENTIFIER() { return getToken(ScribbleParser.IDENTIFIER, 0); }
		public AlphanumericFunctionIdentifierContext(FunctionIdentifierContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterAlphanumericFunctionIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitAlphanumericFunctionIdentifier(this);
		}
	}
	public static class OperatorFunctionIdentifierContext extends FunctionIdentifierContext {
		public TerminalNode OPERATOR() { return getToken(ScribbleParser.OPERATOR, 0); }
		public OperatorFunctionIdentifierContext(FunctionIdentifierContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).enterOperatorFunctionIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ScribbleListener ) ((ScribbleListener)listener).exitOperatorFunctionIdentifier(this);
		}
	}

	public final FunctionIdentifierContext functionIdentifier() throws RecognitionException {
		FunctionIdentifierContext _localctx = new FunctionIdentifierContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_functionIdentifier);
		try {
			setState(455);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPERATOR:
				_localctx = new OperatorFunctionIdentifierContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(453);
				match(OPERATOR);
				}
				break;
			case IDENTIFIER:
				_localctx = new AlphanumericFunctionIdentifierContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(454);
				match(IDENTIFIER);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 11:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 13);
		case 1:
			return precpred(_ctx, 12);
		case 2:
			return precpred(_ctx, 11);
		case 3:
			return precpred(_ctx, 10);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3+\u01cc\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\3\2\7\2\62"+
		"\n\2\f\2\16\2\65\13\2\3\2\3\2\7\29\n\2\f\2\16\2<\13\2\5\2>\n\2\3\2\3\2"+
		"\7\2B\n\2\f\2\16\2E\13\2\5\2G\n\2\3\2\3\2\6\2K\n\2\r\2\16\2L\3\2\7\2P"+
		"\n\2\f\2\16\2S\13\2\5\2U\n\2\3\2\7\2X\n\2\f\2\16\2[\13\2\3\3\3\3\3\3\3"+
		"\3\7\3a\n\3\f\3\16\3d\13\3\3\3\3\3\7\3h\n\3\f\3\16\3k\13\3\3\3\3\3\3\4"+
		"\3\4\3\4\7\4r\n\4\f\4\16\4u\13\4\3\4\3\4\5\4y\n\4\7\4{\n\4\f\4\16\4~\13"+
		"\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\7\5\u0088\n\5\f\5\16\5\u008b\13\5\5"+
		"\5\u008d\n\5\3\5\3\5\7\5\u0091\n\5\f\5\16\5\u0094\13\5\3\5\3\5\7\5\u0098"+
		"\n\5\f\5\16\5\u009b\13\5\7\5\u009d\n\5\f\5\16\5\u00a0\13\5\3\5\5\5\u00a3"+
		"\n\5\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\7\7\u00ae\n\7\f\7\16\7\u00b1"+
		"\13\7\3\7\3\7\5\7\u00b5\n\7\3\7\3\7\5\7\u00b9\n\7\3\7\3\7\3\b\3\b\5\b"+
		"\u00bf\n\b\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00c7\n\t\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\3\13\3\13\3\13\5\13\u00d2\n\13\3\13\3\13\5\13\u00d6\n\13\3\f\3\f\5"+
		"\f\u00da\n\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\5\r\u00eb\n\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\7"+
		"\r\u00fa\n\r\f\r\16\r\u00fd\13\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\7"+
		"\16\u0106\n\16\f\16\16\16\u0109\13\16\3\16\3\16\5\16\u010d\n\16\3\17\3"+
		"\17\3\17\3\17\5\17\u0113\n\17\3\17\3\17\3\17\3\17\7\17\u0119\n\17\f\17"+
		"\16\17\u011c\13\17\3\17\3\17\5\17\u0120\n\17\3\20\3\20\3\20\7\20\u0125"+
		"\n\20\f\20\16\20\u0128\13\20\3\20\3\20\7\20\u012c\n\20\f\20\16\20\u012f"+
		"\13\20\3\20\3\20\7\20\u0133\n\20\f\20\16\20\u0136\13\20\3\20\3\20\7\20"+
		"\u013a\n\20\f\20\16\20\u013d\13\20\7\20\u013f\n\20\f\20\16\20\u0142\13"+
		"\20\3\20\3\20\5\20\u0146\n\20\3\21\3\21\3\21\6\21\u014b\n\21\r\21\16\21"+
		"\u014c\3\21\3\21\3\21\3\21\6\21\u0153\n\21\r\21\16\21\u0154\7\21\u0157"+
		"\n\21\f\21\16\21\u015a\13\21\3\21\3\21\3\22\3\22\7\22\u0160\n\22\f\22"+
		"\16\22\u0163\13\22\3\22\3\22\6\22\u0167\n\22\r\22\16\22\u0168\7\22\u016b"+
		"\n\22\f\22\16\22\u016e\13\22\3\22\3\22\3\23\3\23\3\24\3\24\7\24\u0176"+
		"\n\24\f\24\16\24\u0179\13\24\3\24\3\24\7\24\u017d\n\24\f\24\16\24\u0180"+
		"\13\24\3\24\3\24\7\24\u0184\n\24\f\24\16\24\u0187\13\24\3\24\7\24\u018a"+
		"\n\24\f\24\16\24\u018d\13\24\5\24\u018f\n\24\3\24\7\24\u0192\n\24\f\24"+
		"\16\24\u0195\13\24\3\24\3\24\3\25\3\25\6\25\u019b\n\25\r\25\16\25\u019c"+
		"\3\25\3\25\6\25\u01a1\n\25\r\25\16\25\u01a2\3\25\3\25\6\25\u01a7\n\25"+
		"\r\25\16\25\u01a8\3\25\6\25\u01ac\n\25\r\25\16\25\u01ad\3\25\7\25\u01b1"+
		"\n\25\f\25\16\25\u01b4\13\25\3\25\3\25\6\25\u01b8\n\25\r\25\16\25\u01b9"+
		"\5\25\u01bc\n\25\3\26\3\26\3\27\3\27\3\27\7\27\u01c3\n\27\f\27\16\27\u01c6"+
		"\13\27\3\30\3\30\5\30\u01ca\n\30\3\30\2\3\30\31\2\4\6\b\n\f\16\20\22\24"+
		"\26\30\32\34\36 \"$&(*,.\2\6\4\2\r\r$$\3\2\37 \3\2\21\22\3\2\23\24\2\u0204"+
		"\2\63\3\2\2\2\4\\\3\2\2\2\6n\3\2\2\2\b\u0081\3\2\2\2\n\u00a4\3\2\2\2\f"+
		"\u00a7\3\2\2\2\16\u00bc\3\2\2\2\20\u00c6\3\2\2\2\22\u00c8\3\2\2\2\24\u00ce"+
		"\3\2\2\2\26\u00d7\3\2\2\2\30\u00ea\3\2\2\2\32\u00fe\3\2\2\2\34\u010e\3"+
		"\2\2\2\36\u0121\3\2\2\2 \u0147\3\2\2\2\"\u015d\3\2\2\2$\u0171\3\2\2\2"+
		"&\u0173\3\2\2\2(\u01bb\3\2\2\2*\u01bd\3\2\2\2,\u01bf\3\2\2\2.\u01c9\3"+
		"\2\2\2\60\62\7\'\2\2\61\60\3\2\2\2\62\65\3\2\2\2\63\61\3\2\2\2\63\64\3"+
		"\2\2\2\64=\3\2\2\2\65\63\3\2\2\2\66:\5\4\3\2\679\7\'\2\28\67\3\2\2\29"+
		"<\3\2\2\2:8\3\2\2\2:;\3\2\2\2;>\3\2\2\2<:\3\2\2\2=\66\3\2\2\2=>\3\2\2"+
		"\2>F\3\2\2\2?C\5\6\4\2@B\7\'\2\2A@\3\2\2\2BE\3\2\2\2CA\3\2\2\2CD\3\2\2"+
		"\2DG\3\2\2\2EC\3\2\2\2F?\3\2\2\2FG\3\2\2\2GT\3\2\2\2HQ\5\20\t\2IK\7\'"+
		"\2\2JI\3\2\2\2KL\3\2\2\2LJ\3\2\2\2LM\3\2\2\2MN\3\2\2\2NP\5\20\t\2OJ\3"+
		"\2\2\2PS\3\2\2\2QO\3\2\2\2QR\3\2\2\2RU\3\2\2\2SQ\3\2\2\2TH\3\2\2\2TU\3"+
		"\2\2\2UY\3\2\2\2VX\7\'\2\2WV\3\2\2\2X[\3\2\2\2YW\3\2\2\2YZ\3\2\2\2Z\3"+
		"\3\2\2\2[Y\3\2\2\2\\]\7\3\2\2]^\5,\27\2^b\7(\2\2_a\7\'\2\2`_\3\2\2\2a"+
		"d\3\2\2\2b`\3\2\2\2bc\3\2\2\2ci\3\2\2\2db\3\2\2\2ef\7$\2\2fh\7\'\2\2g"+
		"e\3\2\2\2hk\3\2\2\2ig\3\2\2\2ij\3\2\2\2jl\3\2\2\2ki\3\2\2\2lm\7)\2\2m"+
		"\5\3\2\2\2no\7\4\2\2os\7(\2\2pr\7\'\2\2qp\3\2\2\2ru\3\2\2\2sq\3\2\2\2"+
		"st\3\2\2\2t|\3\2\2\2us\3\2\2\2vx\5,\27\2wy\7\'\2\2xw\3\2\2\2xy\3\2\2\2"+
		"y{\3\2\2\2zv\3\2\2\2{~\3\2\2\2|z\3\2\2\2|}\3\2\2\2}\177\3\2\2\2~|\3\2"+
		"\2\2\177\u0080\7)\2\2\u0080\7\3\2\2\2\u0081\u0082\7\5\2\2\u0082\u008c"+
		"\7$\2\2\u0083\u0084\7\6\2\2\u0084\u0089\7$\2\2\u0085\u0086\7\7\2\2\u0086"+
		"\u0088\7$\2\2\u0087\u0085\3\2\2\2\u0088\u008b\3\2\2\2\u0089\u0087\3\2"+
		"\2\2\u0089\u008a\3\2\2\2\u008a\u008d\3\2\2\2\u008b\u0089\3\2\2\2\u008c"+
		"\u0083\3\2\2\2\u008c\u008d\3\2\2\2\u008d\u00a2\3\2\2\2\u008e\u0092\7("+
		"\2\2\u008f\u0091\7\'\2\2\u0090\u008f\3\2\2\2\u0091\u0094\3\2\2\2\u0092"+
		"\u0090\3\2\2\2\u0092\u0093\3\2\2\2\u0093\u009e\3\2\2\2\u0094\u0092\3\2"+
		"\2\2\u0095\u0099\5\n\6\2\u0096\u0098\7\'\2\2\u0097\u0096\3\2\2\2\u0098"+
		"\u009b\3\2\2\2\u0099\u0097\3\2\2\2\u0099\u009a\3\2\2\2\u009a\u009d\3\2"+
		"\2\2\u009b\u0099\3\2\2\2\u009c\u0095\3\2\2\2\u009d\u00a0\3\2\2\2\u009e"+
		"\u009c\3\2\2\2\u009e\u009f\3\2\2\2\u009f\u00a1\3\2\2\2\u00a0\u009e\3\2"+
		"\2\2\u00a1\u00a3\7)\2\2\u00a2\u008e\3\2\2\2\u00a2\u00a3\3\2\2\2\u00a3"+
		"\t\3\2\2\2\u00a4\u00a5\7$\2\2\u00a5\u00a6\7$\2\2\u00a6\13\3\2\2\2\u00a7"+
		"\u00a8\7\b\2\2\u00a8\u00b4\5.\30\2\u00a9\u00aa\7\t\2\2\u00aa\u00af\5\16"+
		"\b\2\u00ab\u00ac\7\7\2\2\u00ac\u00ae\5\16\b\2\u00ad\u00ab\3\2\2\2\u00ae"+
		"\u00b1\3\2\2\2\u00af\u00ad\3\2\2\2\u00af\u00b0\3\2\2\2\u00b0\u00b2\3\2"+
		"\2\2\u00b1\u00af\3\2\2\2\u00b2\u00b3\7\n\2\2\u00b3\u00b5\3\2\2\2\u00b4"+
		"\u00a9\3\2\2\2\u00b4\u00b5\3\2\2\2\u00b5\u00b8\3\2\2\2\u00b6\u00b7\7\13"+
		"\2\2\u00b7\u00b9\7$\2\2\u00b8\u00b6\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9"+
		"\u00ba\3\2\2\2\u00ba\u00bb\5\"\22\2\u00bb\r\3\2\2\2\u00bc\u00be\7$\2\2"+
		"\u00bd\u00bf\7$\2\2\u00be\u00bd\3\2\2\2\u00be\u00bf\3\2\2\2\u00bf\17\3"+
		"\2\2\2\u00c0\u00c7\5\b\5\2\u00c1\u00c7\5\f\7\2\u00c2\u00c7\5\22\n\2\u00c3"+
		"\u00c7\5\30\r\2\u00c4\u00c7\5\24\13\2\u00c5\u00c7\5\26\f\2\u00c6\u00c0"+
		"\3\2\2\2\u00c6\u00c1\3\2\2\2\u00c6\u00c2\3\2\2\2\u00c6\u00c3\3\2\2\2\u00c6"+
		"\u00c4\3\2\2\2\u00c6\u00c5\3\2\2\2\u00c7\21\3\2\2\2\u00c8\u00c9\7\f\2"+
		"\2\u00c9\u00ca\t\2\2\2\u00ca\u00cb\7\16\2\2\u00cb\u00cc\5\30\r\2\u00cc"+
		"\u00cd\5\"\22\2\u00cd\23\3\2\2\2\u00ce\u00cf\t\3\2\2\u00cf\u00d1\7$\2"+
		"\2\u00d0\u00d2\7$\2\2\u00d1\u00d0\3\2\2\2\u00d1\u00d2\3\2\2\2\u00d2\u00d5"+
		"\3\2\2\2\u00d3\u00d4\7\17\2\2\u00d4\u00d6\5\30\r\2\u00d5\u00d3\3\2\2\2"+
		"\u00d5\u00d6\3\2\2\2\u00d6\25\3\2\2\2\u00d7\u00d9\7$\2\2\u00d8\u00da\7"+
		"%\2\2\u00d9\u00d8\3\2\2\2\u00d9\u00da\3\2\2\2\u00da\u00db\3\2\2\2\u00db"+
		"\u00dc\7\17\2\2\u00dc\u00dd\5\30\r\2\u00dd\27\3\2\2\2\u00de\u00df\b\r"+
		"\1\2\u00df\u00e0\7%\2\2\u00e0\u00eb\5\30\r\20\u00e1\u00eb\5\32\16\2\u00e2"+
		"\u00eb\5\34\17\2\u00e3\u00eb\5\36\20\2\u00e4\u00eb\5 \21\2\u00e5\u00eb"+
		"\5*\26\2\u00e6\u00eb\5(\25\2\u00e7\u00eb\5$\23\2\u00e8\u00eb\5&\24\2\u00e9"+
		"\u00eb\5\"\22\2\u00ea\u00de\3\2\2\2\u00ea\u00e1\3\2\2\2\u00ea\u00e2\3"+
		"\2\2\2\u00ea\u00e3\3\2\2\2\u00ea\u00e4\3\2\2\2\u00ea\u00e5\3\2\2\2\u00ea"+
		"\u00e6\3\2\2\2\u00ea\u00e7\3\2\2\2\u00ea\u00e8\3\2\2\2\u00ea\u00e9\3\2"+
		"\2\2\u00eb\u00fb\3\2\2\2\u00ec\u00ed\f\17\2\2\u00ed\u00ee\5.\30\2\u00ee"+
		"\u00ef\5\30\r\20\u00ef\u00fa\3\2\2\2\u00f0\u00f1\f\16\2\2\u00f1\u00f2"+
		"\7\20\2\2\u00f2\u00fa\5\30\r\17\u00f3\u00f4\f\r\2\2\u00f4\u00f5\t\4\2"+
		"\2\u00f5\u00fa\5\30\r\16\u00f6\u00f7\f\f\2\2\u00f7\u00f8\t\5\2\2\u00f8"+
		"\u00fa\5\30\r\r\u00f9\u00ec\3\2\2\2\u00f9\u00f0\3\2\2\2\u00f9\u00f3\3"+
		"\2\2\2\u00f9\u00f6\3\2\2\2\u00fa\u00fd\3\2\2\2\u00fb\u00f9\3\2\2\2\u00fb"+
		"\u00fc\3\2\2\2\u00fc\31\3\2\2\2\u00fd\u00fb\3\2\2\2\u00fe\u00ff\7\25\2"+
		"\2\u00ff\u0100\5\30\r\2\u0100\u0107\5\"\22\2\u0101\u0102\7\26\2\2\u0102"+
		"\u0103\5\30\r\2\u0103\u0104\5\"\22\2\u0104\u0106\3\2\2\2\u0105\u0101\3"+
		"\2\2\2\u0106\u0109\3\2\2\2\u0107\u0105\3\2\2\2\u0107\u0108\3\2\2\2\u0108"+
		"\u010c\3\2\2\2\u0109\u0107\3\2\2\2\u010a\u010b\7\27\2\2\u010b\u010d\5"+
		"\"\22\2\u010c\u010a\3\2\2\2\u010c\u010d\3\2\2\2\u010d\33\3\2\2\2\u010e"+
		"\u010f\7$\2\2\u010f\u0112\7\30\2\2\u0110\u0113\7$\2\2\u0111\u0113\5\34"+
		"\17\2\u0112\u0110\3\2\2\2\u0112\u0111\3\2\2\2\u0113\u011f\3\2\2\2\u0114"+
		"\u0115\7\t\2\2\u0115\u011a\5\30\r\2\u0116\u0117\7\7\2\2\u0117\u0119\5"+
		"\30\r\2\u0118\u0116\3\2\2\2\u0119\u011c\3\2\2\2\u011a\u0118\3\2\2\2\u011a"+
		"\u011b\3\2\2\2\u011b\u011d\3\2\2\2\u011c\u011a\3\2\2\2\u011d\u011e\7\n"+
		"\2\2\u011e\u0120\3\2\2\2\u011f\u0114\3\2\2\2\u011f\u0120\3\2\2\2\u0120"+
		"\35\3\2\2\2\u0121\u0145\7$\2\2\u0122\u0126\7\t\2\2\u0123\u0125\7\'\2\2"+
		"\u0124\u0123\3\2\2\2\u0125\u0128\3\2\2\2\u0126\u0124\3\2\2\2\u0126\u0127"+
		"\3\2\2\2\u0127\u0129\3\2\2\2\u0128\u0126\3\2\2\2\u0129\u012d\5\30\r\2"+
		"\u012a\u012c\7\'\2\2\u012b\u012a\3\2\2\2\u012c\u012f\3\2\2\2\u012d\u012b"+
		"\3\2\2\2\u012d\u012e\3\2\2\2\u012e\u0140\3\2\2\2\u012f\u012d\3\2\2\2\u0130"+
		"\u0134\7\7\2\2\u0131\u0133\7\'\2\2\u0132\u0131\3\2\2\2\u0133\u0136\3\2"+
		"\2\2\u0134\u0132\3\2\2\2\u0134\u0135\3\2\2\2\u0135\u0137\3\2\2\2\u0136"+
		"\u0134\3\2\2\2\u0137\u013b\5\30\r\2\u0138\u013a\7\'\2\2\u0139\u0138\3"+
		"\2\2\2\u013a\u013d\3\2\2\2\u013b\u0139\3\2\2\2\u013b\u013c\3\2\2\2\u013c"+
		"\u013f\3\2\2\2\u013d\u013b\3\2\2\2\u013e\u0130\3\2\2\2\u013f\u0142\3\2"+
		"\2\2\u0140\u013e\3\2\2\2\u0140\u0141\3\2\2\2\u0141\u0143\3\2\2\2\u0142"+
		"\u0140\3\2\2\2\u0143\u0144\7\n\2\2\u0144\u0146\3\2\2\2\u0145\u0122\3\2"+
		"\2\2\u0145\u0146\3\2\2\2\u0146\37\3\2\2\2\u0147\u0148\7$\2\2\u0148\u014a"+
		"\7(\2\2\u0149\u014b\7\'\2\2\u014a\u0149\3\2\2\2\u014b\u014c\3\2\2\2\u014c"+
		"\u014a\3\2\2\2\u014c\u014d\3\2\2\2\u014d\u0158\3\2\2\2\u014e\u014f\7$"+
		"\2\2\u014f\u0150\7\17\2\2\u0150\u0152\5\30\r\2\u0151\u0153\7\'\2\2\u0152"+
		"\u0151\3\2\2\2\u0153\u0154\3\2\2\2\u0154\u0152\3\2\2\2\u0154\u0155\3\2"+
		"\2\2\u0155\u0157\3\2\2\2\u0156\u014e\3\2\2\2\u0157\u015a\3\2\2\2\u0158"+
		"\u0156\3\2\2\2\u0158\u0159\3\2\2\2\u0159\u015b\3\2\2\2\u015a\u0158\3\2"+
		"\2\2\u015b\u015c\7)\2\2\u015c!\3\2\2\2\u015d\u0161\7(\2\2\u015e\u0160"+
		"\7\'\2\2\u015f\u015e\3\2\2\2\u0160\u0163\3\2\2\2\u0161\u015f\3\2\2\2\u0161"+
		"\u0162\3\2\2\2\u0162\u016c\3\2\2\2\u0163\u0161\3\2\2\2\u0164\u0166\5\20"+
		"\t\2\u0165\u0167\7\'\2\2\u0166\u0165\3\2\2\2\u0167\u0168\3\2\2\2\u0168"+
		"\u0166\3\2\2\2\u0168\u0169\3\2\2\2\u0169\u016b\3\2\2\2\u016a\u0164\3\2"+
		"\2\2\u016b\u016e\3\2\2\2\u016c\u016a\3\2\2\2\u016c\u016d\3\2\2\2\u016d"+
		"\u016f\3\2\2\2\u016e\u016c\3\2\2\2\u016f\u0170\7)\2\2\u0170#\3\2\2\2\u0171"+
		"\u0172\7\31\2\2\u0172%\3\2\2\2\u0173\u0177\7\32\2\2\u0174\u0176\7\'\2"+
		"\2\u0175\u0174\3\2\2\2\u0176\u0179\3\2\2\2\u0177\u0175\3\2\2\2\u0177\u0178"+
		"\3\2\2\2\u0178\u018e\3\2\2\2\u0179\u0177\3\2\2\2\u017a\u017e\5\30\r\2"+
		"\u017b\u017d\7\'\2\2\u017c\u017b\3\2\2\2\u017d\u0180\3\2\2\2\u017e\u017c"+
		"\3\2\2\2\u017e\u017f\3\2\2\2\u017f\u018b\3\2\2\2\u0180\u017e\3\2\2\2\u0181"+
		"\u0185\7\7\2\2\u0182\u0184\7\'\2\2\u0183\u0182\3\2\2\2\u0184\u0187\3\2"+
		"\2\2\u0185\u0183\3\2\2\2\u0185\u0186\3\2\2\2\u0186\u0188\3\2\2\2\u0187"+
		"\u0185\3\2\2\2\u0188\u018a\5\30\r\2\u0189\u0181\3\2\2\2\u018a\u018d\3"+
		"\2\2\2\u018b\u0189\3\2\2\2\u018b\u018c\3\2\2\2\u018c\u018f\3\2\2\2\u018d"+
		"\u018b\3\2\2\2\u018e\u017a\3\2\2\2\u018e\u018f\3\2\2\2\u018f\u0193\3\2"+
		"\2\2\u0190\u0192\7\'\2\2\u0191\u0190\3\2\2\2\u0192\u0195\3\2\2\2\u0193"+
		"\u0191\3\2\2\2\u0193\u0194\3\2\2\2\u0194\u0196\3\2\2\2\u0195\u0193\3\2"+
		"\2\2\u0196\u0197\7\33\2\2\u0197\'\3\2\2\2\u0198\u019a\7\34\2\2\u0199\u019b"+
		"\7!\2\2\u019a\u0199\3\2\2\2\u019b\u019c\3\2\2\2\u019c\u019a\3\2\2\2\u019c"+
		"\u019d\3\2\2\2\u019d\u01bc\3\2\2\2\u019e\u01a0\7\35\2\2\u019f\u01a1\7"+
		"!\2\2\u01a0\u019f\3\2\2\2\u01a1\u01a2\3\2\2\2\u01a2\u01a0\3\2\2\2\u01a2"+
		"\u01a3\3\2\2\2\u01a3\u01bc\3\2\2\2\u01a4\u01a6\7\36\2\2\u01a5\u01a7\7"+
		"!\2\2\u01a6\u01a5\3\2\2\2\u01a7\u01a8\3\2\2\2\u01a8\u01a6\3\2\2\2\u01a8"+
		"\u01a9\3\2\2\2\u01a9\u01bc\3\2\2\2\u01aa\u01ac\7!\2\2\u01ab\u01aa\3\2"+
		"\2\2\u01ac\u01ad\3\2\2\2\u01ad\u01ab\3\2\2\2\u01ad\u01ae\3\2\2\2\u01ae"+
		"\u01bc\3\2\2\2\u01af\u01b1\7!\2\2\u01b0\u01af\3\2\2\2\u01b1\u01b4\3\2"+
		"\2\2\u01b2\u01b0\3\2\2\2\u01b2\u01b3\3\2\2\2\u01b3\u01b5\3\2\2\2\u01b4"+
		"\u01b2\3\2\2\2\u01b5\u01b7\7\30\2\2\u01b6\u01b8\7!\2\2\u01b7\u01b6\3\2"+
		"\2\2\u01b8\u01b9\3\2\2\2\u01b9\u01b7\3\2\2\2\u01b9\u01ba\3\2\2\2\u01ba"+
		"\u01bc\3\2\2\2\u01bb\u0198\3\2\2\2\u01bb\u019e\3\2\2\2\u01bb\u01a4\3\2"+
		"\2\2\u01bb\u01ab\3\2\2\2\u01bb\u01b2\3\2\2\2\u01bc)\3\2\2\2\u01bd\u01be"+
		"\7\"\2\2\u01be+\3\2\2\2\u01bf\u01c4\7$\2\2\u01c0\u01c1\7\30\2\2\u01c1"+
		"\u01c3\7$\2\2\u01c2\u01c0\3\2\2\2\u01c3\u01c6\3\2\2\2\u01c4\u01c2\3\2"+
		"\2\2\u01c4\u01c5\3\2\2\2\u01c5-\3\2\2\2\u01c6\u01c4\3\2\2\2\u01c7\u01ca"+
		"\7%\2\2\u01c8\u01ca\7$\2\2\u01c9\u01c7\3\2\2\2\u01c9\u01c8\3\2\2\2\u01ca"+
		"/\3\2\2\2A\63:=CFLQTYbisx|\u0089\u008c\u0092\u0099\u009e\u00a2\u00af\u00b4"+
		"\u00b8\u00be\u00c6\u00d1\u00d5\u00d9\u00ea\u00f9\u00fb\u0107\u010c\u0112"+
		"\u011a\u011f\u0126\u012d\u0134\u013b\u0140\u0145\u014c\u0154\u0158\u0161"+
		"\u0168\u016c\u0177\u017e\u0185\u018b\u018e\u0193\u019c\u01a2\u01a8\u01ad"+
		"\u01b2\u01b9\u01bb\u01c4\u01c9";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}