// Generated from /home/niklas/Projects/personal/scribble/grammar/Scribble.g4 by ANTLR 4.8
package antlr4;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ScribbleParser}.
 */
public interface ScribbleListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#scribble}.
	 * @param ctx the parse tree
	 */
	void enterScribble(ScribbleParser.ScribbleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#scribble}.
	 * @param ctx the parse tree
	 */
	void exitScribble(ScribbleParser.ScribbleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#moduleDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterModuleDeclaration(ScribbleParser.ModuleDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#moduleDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitModuleDeclaration(ScribbleParser.ModuleDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#importDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterImportDeclaration(ScribbleParser.ImportDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#importDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitImportDeclaration(ScribbleParser.ImportDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#recordDefinition}.
	 * @param ctx the parse tree
	 */
	void enterRecordDefinition(ScribbleParser.RecordDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#recordDefinition}.
	 * @param ctx the parse tree
	 */
	void exitRecordDefinition(ScribbleParser.RecordDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#recordMemberDefinition}.
	 * @param ctx the parse tree
	 */
	void enterRecordMemberDefinition(ScribbleParser.RecordMemberDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#recordMemberDefinition}.
	 * @param ctx the parse tree
	 */
	void exitRecordMemberDefinition(ScribbleParser.RecordMemberDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDefinition(ScribbleParser.FunctionDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDefinition(ScribbleParser.FunctionDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#functionParameter}.
	 * @param ctx the parse tree
	 */
	void enterFunctionParameter(ScribbleParser.FunctionParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#functionParameter}.
	 * @param ctx the parse tree
	 */
	void exitFunctionParameter(ScribbleParser.FunctionParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(ScribbleParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(ScribbleParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#loop}.
	 * @param ctx the parse tree
	 */
	void enterLoop(ScribbleParser.LoopContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#loop}.
	 * @param ctx the parse tree
	 */
	void exitLoop(ScribbleParser.LoopContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#variableDefinition}.
	 * @param ctx the parse tree
	 */
	void enterVariableDefinition(ScribbleParser.VariableDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#variableDefinition}.
	 * @param ctx the parse tree
	 */
	void exitVariableDefinition(ScribbleParser.VariableDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#variableAssignment}.
	 * @param ctx the parse tree
	 */
	void enterVariableAssignment(ScribbleParser.VariableAssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#variableAssignment}.
	 * @param ctx the parse tree
	 */
	void exitVariableAssignment(ScribbleParser.VariableAssignmentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ConstructorInvocationE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterConstructorInvocationE(ScribbleParser.ConstructorInvocationEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ConstructorInvocationE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitConstructorInvocationE(ScribbleParser.ConstructorInvocationEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StringLiteralE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterStringLiteralE(ScribbleParser.StringLiteralEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StringLiteralE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitStringLiteralE(ScribbleParser.StringLiteralEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ConditionalE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterConditionalE(ScribbleParser.ConditionalEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ConditionalE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitConditionalE(ScribbleParser.ConditionalEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StdInfixExpression}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterStdInfixExpression(ScribbleParser.StdInfixExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StdInfixExpression}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitStdInfixExpression(ScribbleParser.StdInfixExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code PrefixExpression}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPrefixExpression(ScribbleParser.PrefixExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code PrefixExpression}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPrefixExpression(ScribbleParser.PrefixExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code RecordMemberSelectionE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterRecordMemberSelectionE(ScribbleParser.RecordMemberSelectionEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code RecordMemberSelectionE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitRecordMemberSelectionE(ScribbleParser.RecordMemberSelectionEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BlockOfCodeE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBlockOfCodeE(ScribbleParser.BlockOfCodeEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BlockOfCodeE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBlockOfCodeE(ScribbleParser.BlockOfCodeEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code InfixExpression}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterInfixExpression(ScribbleParser.InfixExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code InfixExpression}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitInfixExpression(ScribbleParser.InfixExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code NumberLiteralE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNumberLiteralE(ScribbleParser.NumberLiteralEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NumberLiteralE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNumberLiteralE(ScribbleParser.NumberLiteralEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code SymbolReferenceE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSymbolReferenceE(ScribbleParser.SymbolReferenceEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SymbolReferenceE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSymbolReferenceE(ScribbleParser.SymbolReferenceEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ListLiteralE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterListLiteralE(ScribbleParser.ListLiteralEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ListLiteralE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitListLiteralE(ScribbleParser.ListLiteralEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MapLiteralE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMapLiteralE(ScribbleParser.MapLiteralEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MapLiteralE}
	 * labeled alternative in {@link ScribbleParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMapLiteralE(ScribbleParser.MapLiteralEContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#conditional}.
	 * @param ctx the parse tree
	 */
	void enterConditional(ScribbleParser.ConditionalContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#conditional}.
	 * @param ctx the parse tree
	 */
	void exitConditional(ScribbleParser.ConditionalContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#recordMemberSelection}.
	 * @param ctx the parse tree
	 */
	void enterRecordMemberSelection(ScribbleParser.RecordMemberSelectionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#recordMemberSelection}.
	 * @param ctx the parse tree
	 */
	void exitRecordMemberSelection(ScribbleParser.RecordMemberSelectionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#symbolReference}.
	 * @param ctx the parse tree
	 */
	void enterSymbolReference(ScribbleParser.SymbolReferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#symbolReference}.
	 * @param ctx the parse tree
	 */
	void exitSymbolReference(ScribbleParser.SymbolReferenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#constructorInvocation}.
	 * @param ctx the parse tree
	 */
	void enterConstructorInvocation(ScribbleParser.ConstructorInvocationContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#constructorInvocation}.
	 * @param ctx the parse tree
	 */
	void exitConstructorInvocation(ScribbleParser.ConstructorInvocationContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#blockOfCode}.
	 * @param ctx the parse tree
	 */
	void enterBlockOfCode(ScribbleParser.BlockOfCodeContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#blockOfCode}.
	 * @param ctx the parse tree
	 */
	void exitBlockOfCode(ScribbleParser.BlockOfCodeContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#mapLiteral}.
	 * @param ctx the parse tree
	 */
	void enterMapLiteral(ScribbleParser.MapLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#mapLiteral}.
	 * @param ctx the parse tree
	 */
	void exitMapLiteral(ScribbleParser.MapLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#listLiteral}.
	 * @param ctx the parse tree
	 */
	void enterListLiteral(ScribbleParser.ListLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#listLiteral}.
	 * @param ctx the parse tree
	 */
	void exitListLiteral(ScribbleParser.ListLiteralContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BinaryNumberLiteral}
	 * labeled alternative in {@link ScribbleParser#numberLiteral}.
	 * @param ctx the parse tree
	 */
	void enterBinaryNumberLiteral(ScribbleParser.BinaryNumberLiteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BinaryNumberLiteral}
	 * labeled alternative in {@link ScribbleParser#numberLiteral}.
	 * @param ctx the parse tree
	 */
	void exitBinaryNumberLiteral(ScribbleParser.BinaryNumberLiteralContext ctx);
	/**
	 * Enter a parse tree produced by the {@code HexNumberLiteral}
	 * labeled alternative in {@link ScribbleParser#numberLiteral}.
	 * @param ctx the parse tree
	 */
	void enterHexNumberLiteral(ScribbleParser.HexNumberLiteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code HexNumberLiteral}
	 * labeled alternative in {@link ScribbleParser#numberLiteral}.
	 * @param ctx the parse tree
	 */
	void exitHexNumberLiteral(ScribbleParser.HexNumberLiteralContext ctx);
	/**
	 * Enter a parse tree produced by the {@code OctNumberLiteral}
	 * labeled alternative in {@link ScribbleParser#numberLiteral}.
	 * @param ctx the parse tree
	 */
	void enterOctNumberLiteral(ScribbleParser.OctNumberLiteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code OctNumberLiteral}
	 * labeled alternative in {@link ScribbleParser#numberLiteral}.
	 * @param ctx the parse tree
	 */
	void exitOctNumberLiteral(ScribbleParser.OctNumberLiteralContext ctx);
	/**
	 * Enter a parse tree produced by the {@code DecimalInteger}
	 * labeled alternative in {@link ScribbleParser#numberLiteral}.
	 * @param ctx the parse tree
	 */
	void enterDecimalInteger(ScribbleParser.DecimalIntegerContext ctx);
	/**
	 * Exit a parse tree produced by the {@code DecimalInteger}
	 * labeled alternative in {@link ScribbleParser#numberLiteral}.
	 * @param ctx the parse tree
	 */
	void exitDecimalInteger(ScribbleParser.DecimalIntegerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code DecimalFloatingPoint}
	 * labeled alternative in {@link ScribbleParser#numberLiteral}.
	 * @param ctx the parse tree
	 */
	void enterDecimalFloatingPoint(ScribbleParser.DecimalFloatingPointContext ctx);
	/**
	 * Exit a parse tree produced by the {@code DecimalFloatingPoint}
	 * labeled alternative in {@link ScribbleParser#numberLiteral}.
	 * @param ctx the parse tree
	 */
	void exitDecimalFloatingPoint(ScribbleParser.DecimalFloatingPointContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#stringLiteral}.
	 * @param ctx the parse tree
	 */
	void enterStringLiteral(ScribbleParser.StringLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#stringLiteral}.
	 * @param ctx the parse tree
	 */
	void exitStringLiteral(ScribbleParser.StringLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link ScribbleParser#fullyQualifiedModuleName}.
	 * @param ctx the parse tree
	 */
	void enterFullyQualifiedModuleName(ScribbleParser.FullyQualifiedModuleNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link ScribbleParser#fullyQualifiedModuleName}.
	 * @param ctx the parse tree
	 */
	void exitFullyQualifiedModuleName(ScribbleParser.FullyQualifiedModuleNameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code OperatorFunctionIdentifier}
	 * labeled alternative in {@link ScribbleParser#functionIdentifier}.
	 * @param ctx the parse tree
	 */
	void enterOperatorFunctionIdentifier(ScribbleParser.OperatorFunctionIdentifierContext ctx);
	/**
	 * Exit a parse tree produced by the {@code OperatorFunctionIdentifier}
	 * labeled alternative in {@link ScribbleParser#functionIdentifier}.
	 * @param ctx the parse tree
	 */
	void exitOperatorFunctionIdentifier(ScribbleParser.OperatorFunctionIdentifierContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AlphanumericFunctionIdentifier}
	 * labeled alternative in {@link ScribbleParser#functionIdentifier}.
	 * @param ctx the parse tree
	 */
	void enterAlphanumericFunctionIdentifier(ScribbleParser.AlphanumericFunctionIdentifierContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AlphanumericFunctionIdentifier}
	 * labeled alternative in {@link ScribbleParser#functionIdentifier}.
	 * @param ctx the parse tree
	 */
	void exitAlphanumericFunctionIdentifier(ScribbleParser.AlphanumericFunctionIdentifierContext ctx);
}