package scribble

import antlr4.{ScribbleLexer, ScribbleParser}
import org.antlr.v4.runtime.{ANTLRInputStream, CommonTokenStream}
import org.antlr.v4.runtime.tree.{ParseTree, ParseTreeWalker}
import scribble.ast.AbstractSyntaxTree
import scribble.parsed.listeners.AstBuilder

import java.io.{ByteArrayInputStream, InputStream}
import scala.util.{Failure, Success, Using}


class ScribbleFrontend {

  def parse(inputStream: InputStream): AbstractSyntaxTree = {
    val input = new ANTLRInputStream(inputStream)
    // create a lexer that feeds off of input CharStream
    val lexer = new ScribbleLexer(input)
    // create a buffer of tokens pulled from the lexer
    val tokens = new CommonTokenStream(lexer)
    // create a parser that feeds off the tokens buffer
    val parser = new ScribbleParser(tokens)

    buildAst(parser.scribble)
  }

  def parse(source: String): AbstractSyntaxTree =
    Using(new ByteArrayInputStream(source.getBytes))(parse) match {
      case Success(value) => value
      case Failure(exception) => throw exception
    }

  private def buildAst(tree: ParseTree) = {
    val walker = new ParseTreeWalker
    val astBuilder = new AstBuilder
    walker.walk(astBuilder, tree)
    astBuilder.partialTrees.get(tree)
  }

}
