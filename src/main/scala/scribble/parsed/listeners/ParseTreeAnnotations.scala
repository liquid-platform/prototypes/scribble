package scribble.parsed.listeners

import antlr4.ScribbleBaseListener
import org.antlr.v4.runtime.tree.{ParseTree, ParseTreeProperty}
import scribble.ast._

trait ParseTreeAnnotations {

  val partialTrees = new ParseTreeProperty[AbstractSyntaxTree]

  protected def annotate(tree: ParseTree, ast: AbstractSyntaxTree): Unit =
    partialTrees.put(tree, ast)

  def annotationFor[A <: Node](tree: ParseTree): Option[A] =
    Option(partialTrees.get(tree).asInstanceOf[A])

}
