package scribble.parsed.listeners

import antlr4._
import scribble.ast._
import scribble.util.syntax._

import scala.jdk.CollectionConverters._

class AstBuilder extends ScribbleBaseListener with ParseTreeAnnotations {

  protected implicit val listenerW = this

  override def exitModuleDeclaration(ctx: ScribbleParser.ModuleDeclarationContext) =
    annotate(ctx,
      ModuleDeclaration(
        name = ModuleName(ctx.IDENTIFIER.asScala.map(_.toAstIdentifier).toList),
        exports = ctx.IDENTIFIER.asScala.map(_.toAstIdentifier).toList
      )
    )

  override def exitRecordDefinition(ctx: ScribbleParser.RecordDefinitionContext) = {
    val recordName :: inheritances = ctx.IDENTIFIER.asScala.toList.map(_.toAstIdentifier)
    annotate(ctx,
      RecordDefinition(
        recordName,
        inheritances,
        members = ctx.recordMemberDefinition.asScala.toList.map { rmd =>
          val memberName = rmd.IDENTIFIER(0).toAstIdentifier
          val memberType = rmd.IDENTIFIER(1).toAstIdentifier
          (memberName, memberType)
        }
      )
    )
  }

  override def exitFunctionDefinition(ctx: ScribbleParser.FunctionDefinitionContext) =
    annotate(ctx,
      FunctionDefinition(
        name = Identifier(ctx.functionIdentifier.getText),
        parameters = ctx.functionParameter.asScala.toList.map { p =>
          FunctionParameter(
            name = p.IDENTIFIER(0).toAstIdentifier,
            `type` = p.IDENTIFIER(1).toAstIdentifierOption
          )
        },
        returnType = ctx.IDENTIFIER.toAstIdentifierOption,
        body = annotationFor[BlockOfCode](ctx.blockOfCode).get
      )
    )

  override def exitLoop(ctx: ScribbleParser.LoopContext) =
    annotate(ctx,
      Loop(
        binder = ctx.IDENTIFIER.toAstIdentifierOption,
        iteratee = annotationFor[Expression](ctx.expression).get,
        body = annotationFor[BlockOfCode](ctx.blockOfCode).get
      )
    )

  override def exitVariableDefinition(ctx: ScribbleParser.VariableDefinitionContext) =
    annotate(ctx,
      VariableDefinition(
        mutable = ctx.key.getType match {
          case ScribbleLexer.MUTABLE_VAR_DEF => true
          case ScribbleLexer.IMMUTABLE_VAR_DEF => false
          case _ => throw new IllegalStateException("LMAO I got u future nik")
        },
        identifier = ctx.IDENTIFIER(0).toAstIdentifier,
        `type` = ctx.IDENTIFIER(1).toAstIdentifierOption,
        initializer = annotationFor[Expression](ctx.expression)
      )
    )

  override def exitVariableAssignment(ctx: ScribbleParser.VariableAssignmentContext) = {
    val targetVariable = ctx.IDENTIFIER.toAstIdentifier
    val expr = annotationFor[Expression](ctx.expression).get

    // if this assignment is a compound operation such as
    // += or *= or any other operator
    // swap `a OP= b` with `a = a OP b`
    val assignedValue =
    if (ctx.OPERATOR != null)
      InfixOperation(
        left = SymbolReference(targetVariable),
        operator = Operator(ctx.OPERATOR.getText),
        right = expr
      )
    else expr

    annotate(ctx,
      VariableAssignment(
        targetVariable,
        assignedValue
      )
    )
  }

  override def exitPrefixExpression(ctx: ScribbleParser.PrefixExpressionContext) =
    annotate(ctx,
      PrefixOperation(
        operand = annotationFor[Expression](ctx.expression).get,
        operator = Operator(ctx.OPERATOR.getText)
      )
    )

  override def exitInfixExpression(ctx: ScribbleParser.InfixExpressionContext) =
    annotate(ctx,
      InfixOperation(
        left = annotationFor[Expression](ctx.expression(0)).get,
        operator = Operator(ctx.functionIdentifier.getText),
        right = annotationFor[Expression](ctx.expression(1)).get
      ): InfixOperation
    )

  override def exitStdInfixExpression(ctx: ScribbleParser.StdInfixExpressionContext) =
    annotate(ctx,
      InfixOperation(
        left = annotationFor[Expression](ctx.expression(0)).get,
        operator = Operator(ctx.op.getText),
        right = annotationFor[Expression](ctx.expression(1)).get
      )
    )

  override def exitConditionalE(ctx: ScribbleParser.ConditionalEContext) = {
    val cond = ctx.conditional
    annotate(ctx,
      Conditional(
        `if` = (
          annotationFor[Expression](cond.ifCondition).get,
          annotationFor[BlockOfCode](cond.ifBranchBody).get
        ),
        `else ifs` = null, // TODO lol, couldn't figure out how to index named tokens
        `else` = annotationFor[BlockOfCode](cond.elseBranchBody)
      )
    )
  }

  override def exitRecordMemberSelectionE(ctx: ScribbleParser.RecordMemberSelectionEContext) = {
    val rms = ctx.recordMemberSelection
    annotate(ctx,
      RecordMemberSelection(
        recordSymbol = rms.IDENTIFIER(0).toAstIdentifier,
        selector =
          if (rms.IDENTIFIER != null) Right(rms.IDENTIFIER(1).toAstIdentifier)
          else Left(annotationFor[RecordMemberSelection](rms.recordMemberSelection).get),
        arguments = rms.expression.asScala.map(annotationFor[Expression](_).get).toList
      )
    )
  }

  override def exitSymbolReferenceE(ctx: ScribbleParser.SymbolReferenceEContext) = {
    val sr = ctx.symbolReference
    annotate(ctx,
      SymbolReference(
        name = sr.IDENTIFIER.toAstIdentifier,
        arguments =
          Option(sr.expression)
            .map(_.asScala.map(annotationFor[Expression](_).get).toList)
            .getOrElse(Nil)
      )
    )
  }

  override def exitConstructorInvocationE(ctx: ScribbleParser.ConstructorInvocationEContext) = {
    val ci = ctx.constructorInvocation
    val associatedType :: fieldNames = ci.IDENTIFIER.asScala.map(_.toAstIdentifier).toList
    annotate(ctx,
      RecordConstructorInvocation(
        associatedType,
        initializations = fieldNames.zipWithIndex.map { case (fieldName, index) =>
          (fieldName, annotationFor[Expression](ci.expression(index)).get)
        }
      )
    )
  }

  override def exitMapLiteralE(ctx: ScribbleParser.MapLiteralEContext) =
    annotate(ctx, MapLiteral(Nil)) // TODO

  override def exitListLiteralE(ctx: ScribbleParser.ListLiteralEContext) = {
    val ll = ctx.listLiteral
    annotate(ctx,
      ListLiteral(
        elements = ll.expression.asScala.map(annotationFor[Expression](_).get).toList
      )
    )
  }

  override def exitNumberLiteralE(ctx: ScribbleParser.NumberLiteralEContext) =
    annotate(ctx, annotationFor[Expression](ctx.numberLiteral).get)

  override def exitBinaryNumberLiteral(ctx: ScribbleParser.BinaryNumberLiteralContext) =
    annotate(ctx, BinaryLiteral(ctx.DIGIT.asScala.mkString))

  override def exitHexNumberLiteral(ctx: ScribbleParser.HexNumberLiteralContext) =
    annotate(ctx, HexadecimalLiteral(ctx.DIGIT.asScala.mkString))

  override def exitOctNumberLiteral(ctx: ScribbleParser.OctNumberLiteralContext) =
    annotate(ctx, OctalLiteral(ctx.DIGIT.asScala.mkString))

  override def exitDecimalInteger(ctx: ScribbleParser.DecimalIntegerContext) =
    annotate(ctx, DecimalLiteral(ctx.DIGIT.asScala.mkString))

  override def exitDecimalFloatingPoint(ctx: ScribbleParser.DecimalFloatingPointContext) =
    annotate(ctx,
      DecimalLiteral(
        digitsBeforeComma = Option(ctx.beforeComma).map(_.getText).getOrElse(""),
        digitsAfterComma = ctx.afterComma.getText
      )
    )

  override def exitStringLiteralE(ctx: ScribbleParser.StringLiteralEContext) = {
    val sl = ctx.stringLiteral
    annotate(ctx, StringLiteral(sl.STRING.getText))
  }

  override def exitBlockOfCode(ctx: ScribbleParser.BlockOfCodeContext) = {
    import cats.implicits._
    val definition =
      BlockOfCode(
        statements =
          if (ctx.statement != null)
            ctx.statement.asScala.map(stmt => annotationFor[Statement](stmt.getChild(0)))
               .toList.sequence.getOrElse(Nil)
          else Nil
      )
    annotate(ctx, definition)
  }

  override def exitBlockOfCodeE(ctx: ScribbleParser.BlockOfCodeEContext) = {
    annotate(ctx, annotationFor[BlockOfCode](ctx.blockOfCode).get)
  }

  override def exitScribble(ctx: ScribbleParser.ScribbleContext) = {
    val moduleDeclaration = annotationFor[ModuleDeclaration](ctx.moduleDeclaration)
    val importDeclaration = annotationFor[ImportDeclaration](ctx.importDeclaration)
    val statements = ctx.statement.asScala.map(stmt => annotationFor[Statement](stmt.getChild(0)).get).toList
    val compilationUnit = ScribbleCompilationUnit(moduleDeclaration, importDeclaration, statements)
    annotate(ctx, compilationUnit)
  }

}
