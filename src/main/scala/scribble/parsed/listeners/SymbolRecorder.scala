package scribble.parsed.listeners

import antlr4.{ScribbleBaseListener, ScribbleParser}

import collection.mutable

class SymbolRecorder extends ScribbleBaseListener {
  import SymbolRecorder._

  val symbols = mutable.Set.empty[Symbol]

  override def exitRecordDefinition(ctx: ScribbleParser.RecordDefinitionContext) =
    symbols += Symbol(ctx.IDENTIFIER(0).getText, Record)

  override def exitOperatorFunctionIdentifier(ctx: ScribbleParser.OperatorFunctionIdentifierContext) =
    symbols += Symbol(ctx.OPERATOR().getText, Function)

  override def exitAlphanumericFunctionIdentifier(ctx: ScribbleParser.AlphanumericFunctionIdentifierContext) =
    symbols += Symbol(ctx.IDENTIFIER().getText, Function)

  override def exitFunctionParameter(ctx: ScribbleParser.FunctionParameterContext) =
    symbols += Symbol(ctx.IDENTIFIER(0).getText, FunctionParameter)

  override def exitVariableDefinition(ctx: ScribbleParser.VariableDefinitionContext) =
    symbols += Symbol(
      ctx.IDENTIFIER(0).getText,
      if (ctx.key.getType == ScribbleParser.IMMUTABLE_VAR_DEF)
        ImmutableVariable
      else
        MutableVariable
    )

}

object SymbolRecorder {

  sealed trait SymbolKind
  case object Function extends SymbolKind
  case object FunctionParameter extends SymbolKind
  case object MutableVariable extends SymbolKind
  case object ImmutableVariable extends SymbolKind
  case object Record extends SymbolKind

  case class Symbol(name: String, kind: SymbolKind)

}
