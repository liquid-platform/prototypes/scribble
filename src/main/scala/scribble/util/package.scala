package scribble

import antlr4.ScribbleParser
import cats.implicits._
import org.antlr.v4.runtime.tree.TerminalNode
import org.antlr.v4.runtime.Token
import scribble.ast._
import scribble.parsed.listeners.AstBuilder

import scala.jdk.CollectionConverters._

package object util {

  object syntax {

    implicit class TerminalNodeIdentifierSyntax(n: TerminalNode) {
      def toAstIdentifier =
        Identifier(n.getText)

      def toAstIdentifierOption =
        Option(n).map(_.toAstIdentifier)
    }

    implicit class TokenIdentifierSyntax(t: Token) {
      def toAstIdentifier =
        Identifier(t.getText)

      def toAstIdentifierOption =
        Option(t).map(_.toAstIdentifier)
    }

    implicit class StatementsSyntax(ctx: ScribbleParser.BlockOfCodeContext) {
      def extractStatements(implicit ab: AstBuilder) =
        extractStatementsOption.get

      def extractStatementsOption(implicit ab: AstBuilder) = {
        if (ctx != null && ctx.statement != null)
          ctx.statement.asScala
             .map(ab.annotationFor[Statement]).toList.sequence
             .map(BlockOfCode)
        else
          None

      }
    }

  }

}
