package scribble.ast

case class Conditional(
  `if`: (Expression, BlockOfCode),
  `else ifs`: List[(Expression, BlockOfCode)],
  `else`: Option[BlockOfCode]
) extends Expression

case class Loop(
  binder: Option[Identifier],
  iteratee: Expression,
  body: BlockOfCode
) extends Statement
