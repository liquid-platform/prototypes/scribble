package scribble.ast

case class VariableDefinition(
  mutable: Boolean,
  identifier: Identifier,
  `type`: Option[Identifier],
  // mutable variables can be initialized to 0-value of respective type
  initializer: Option[Expression]
) extends Statement

// compound assignments like x+=3 can be compiled to
// VariableAssignment(x, x+3), no need for compound assignment nodes
case class VariableAssignment(
  targetVariable: Identifier,
  assignedValue: Expression
) extends Statement
