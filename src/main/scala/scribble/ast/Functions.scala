package scribble.ast

case class FunctionParameter(
  name: Identifier,
  `type`: Option[Identifier]
) extends Node

case class FunctionDefinition(
  name: Identifier,
  parameters: List[FunctionParameter],
  returnType: Option[Identifier],
  body: BlockOfCode
) extends Statement