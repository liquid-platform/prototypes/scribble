package scribble.ast

trait Node

case class Identifier(name: String) extends Node

case class ModuleName(segments: List[Identifier]) extends Node

case class Operator(op: String) extends Node

case class ScribbleCompilationUnit(
  moduleDeclaration: Option[ModuleDeclaration],
  importDeclaration: Option[ImportDeclaration],
  statements: List[Statement]
) extends Node

case class ModuleDeclaration(name: ModuleName, exports: List[Identifier]) extends Node

case class ImportDeclaration(imports: List[ModuleName]) extends Node

trait Statement extends Node

trait Expression extends Statement

case class InfixOperation(
  left: Expression,
  operator: Operator,
  right: Expression,
) extends Expression

case class PrefixOperation(
  operand: Expression,
  operator: Operator
) extends Expression

case class SymbolReference(
  name: Identifier,
  arguments: List[Expression] = Nil
) extends Expression

case class BlockOfCode(statements: List[Statement]) extends Expression
