package scribble.ast

case class RecordDefinition(
  name: Identifier,
  derivations: List[Identifier],
  members: List[(Identifier, Identifier)]
) extends Statement

case class RecordMemberSelection(
  recordSymbol: Identifier,
  selector: Either[RecordMemberSelection, Identifier],
  // for any `fun f(x, y)`, x.f(y) is also a member selection.
  // maybe this should be a dedicated AST node
  arguments: List[Expression]
) extends Expression

case class RecordConstructorInvocation(
  associatedType: Identifier,
  initializations: List[(Identifier, Expression)]
) extends Expression