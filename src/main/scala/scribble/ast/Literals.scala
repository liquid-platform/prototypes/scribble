package scribble.ast

sealed trait NumberLiteral extends Expression

case class BinaryLiteral(digits: String) extends NumberLiteral

case class HexadecimalLiteral(digits: String) extends NumberLiteral

case class OctalLiteral(digits: String) extends NumberLiteral

case class DecimalLiteral(
  var digitsBeforeComma: String,
  var digitsAfterComma: String = ""
) extends NumberLiteral

case class StringLiteral(
  contents: String
) extends Expression

// TODO list comprehensions
case class ListLiteral(
  var elements: List[Expression]
) extends Expression

case class MapLiteral(
  var tuples: List[(Expression, Expression)]
) extends Expression

