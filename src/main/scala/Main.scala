import scribble._

object Main {

  def main(args: Array[String]) = {

    val input =
//      getClass.getClassLoader.getResourceAsStream("testfile.sm")
      """
        |record Person {
        |  name String
        |  age  Int
        |}""".stripMargin

    val c = new ScribbleFrontend
    val ast = c.parse(input)

    println(ast)
  }

}
