parser grammar ScribbleParser;

options { tokenVocab=ScribbleLexer; }

scribble :
    ( moduleDeclaration NL*)? // module declarations on top, can be left out for application files
    ( importDeclaration NL*)?
    ( statement ( NL+ statement )* )?
    NL*
;

moduleDeclaration :
    'module' fullyQualifiedModuleName SYM_SCOPE_OPEN NL*
        ( ( VARIABLE_IDENTIFIER | TYPE_IDENTIFIER /* symbols to export */ ) NL )*
    SYM_SCOPE_CLOSE
;

importDeclaration :
    'import' SYM_SCOPE_OPEN NL*
        ( fullyQualifiedModuleName NL? )*
    SYM_SCOPE_CLOSE
;

recordDefinition :
    'record' TYPE_IDENTIFIER
    ( ':' TYPE_IDENTIFIER (',' TYPE_IDENTIFIER)* )? // inheriting props
    ( SYM_SCOPE_OPEN NL*
        ( recordMemberDefinition NL* )*
    SYM_SCOPE_CLOSE )? // records without { } are allowed
;

recordMemberDefinition :
    VARIABLE_IDENTIFIER /* name */ TYPE_IDENTIFIER /* type */
;

functionDefinition :
    'op'? 'fun' ( functionIdentifier ) // declares the name
    ( '(' functionParameter (',' functionParameter)* ')' )? // params are optional
    ( '->' TYPE_IDENTIFIER )? // return type is optional
    blockOfCode
;

functionParameter : VARIABLE_IDENTIFIER /* name */ TYPE_IDENTIFIER? /* type (optional) */ ;

statement :
    ( recordDefinition
    | functionDefinition
    | loop
    | expression // functionCall, arithmetic, string, etc.
    | constVariableDefinition
    | mutableVariableDefinition
    | mutableVariableAssignment )
;

// TODO maybe promote to expression?
loop :
    'foreach' ( VARIABLE_IDENTIFIER | '_' ) /* variable introduction */ 'in'
    expression /* iterable */
    blockOfCode
;

constVariableDefinition :
    'let' VARIABLE_IDENTIFIER // declares the name
    TYPE_IDENTIFIER? // type can be inferred
    '=' expression
;

mutableVariableDefinition :
    'var' VARIABLE_IDENTIFIER // declares the name
    TYPE_IDENTIFIER? // type can be inferred
    '=' expression
;

mutableVariableAssignment :
    VARIABLE_IDENTIFIER OPERATOR? '=' expression
;

expression :
    OPERATOR expression                             // precedence P1: prefix
    | expression functionIdentifier expression      // precedence P2: infix
    | expression '^'<assoc=right> expression        // precedence P3: exponentiation
    | expression ( '*' | '/' ) expression           // precedence P4: mult/div
    | expression  ( '+' | '-' ) expression          // precedence P5: add/sub
    | conditional
    | recordMemberSelection
    | symbolReference
    | constructorInvocation
    | stringLiteral
    | numberLiteral
    | mapLiteral
    | listLiteral
    | blockOfCode // scoped code
;

//expression :
//    term ( ( '+' | '-' ) term )*
//;
//
//term :
//    factor ( ( '*' | '/' ) factor )*
//;
//
//factor :
//    powered ( '^'<assoc=right> powered )*
//;
//
//powered :
//    infixOperand ( infixOperator infixOperand )*
//;
//
//infixOperand :
//    OPERATOR? prefixOperand
//;

//prefixOperand :
//    conditional
//    | recordMemberSelection
//    | symbolReference
//    | constructorInvocation
//    | stringLiteral
//    | numberLiteral
//    | mapLiteral
//    | listLiteral
//    | blockOfCode // scoped code
//;

conditional :
    'if' expression blockOfCode
    ( 'else if' expression blockOfCode )* // else-if is optional
    ( 'else' blockOfCode )? // else is optional
;

recordMemberSelection :
    VARIABLE_IDENTIFIER '.' ( VARIABLE_IDENTIFIER | recordMemberSelection )
    // for any `fun f(x, y)`, x.f(y) is also a member selection
    ( '(' expression ( ',' expression )* /* arguments */ ')' )?
;

symbolReference :
    VARIABLE_IDENTIFIER ( '(' NL* expression NL* ( ',' NL* expression NL* )* /* arguments */ ')' )?
;

constructorInvocation :
    TYPE_IDENTIFIER SYM_SCOPE_OPEN NL+
        ( VARIABLE_IDENTIFIER '=' expression NL+ )* // 0 or more initializations
    SYM_SCOPE_CLOSE
;

blockOfCode :
    SYM_SCOPE_OPEN NL*
        ( statement NL+ )*
    SYM_SCOPE_CLOSE
;

/*
    LITERALS
*/

// TODO
mapLiteral :
    ('(:)' /* empty map */ )
;

// TODO list comprehensions
listLiteral :
    '[' NL* ( expression NL* ( ',' NL* expression )* )? NL* ']'
;

// TODO optimize
//  -> move number recognition to lexer
numberLiteral :
    ( '0b' DIGIT+ )   // binary
    | ( '0x' DIGIT+ ) // hex
    | ( '0o' DIGIT+ ) // octal
    | ( DIGIT+ ) // regular integer
    | ( DIGIT* '.' DIGIT+ )// floating point number
;

stringLiteral :
   STRING_DELIMETER {insideString = true;} stringComponent* '"' {insideString = false;}
;

stringComponent :
    REGULAR_STRING_COMPONENT #RegularStringComponent
   | STRING_INTERPOLATION #StringInterpolation
;



/*
    IDENTIFIERS & NAMES
*/

// allow nested modules
fullyQualifiedModuleName :
    VARIABLE_IDENTIFIER ( '.' VARIABLE_IDENTIFIER )*
;

functionIdentifier :
    OPERATOR                # OperatorFunctionIdentifier
    | VARIABLE_IDENTIFIER   # AlphanumericFunctionIdentifier
;
