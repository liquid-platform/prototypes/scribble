grammar Scribble;

scribble :
    NL*
    ( moduleDeclaration NL*)? // module declarations on top, can be left out for application files
    ( importDeclaration NL*)?
    ( statement ( NL+ statement )* )?
    NL*
;

moduleDeclaration :
    'module' fullyQualifiedModuleName SYM_SCOPE_OPEN NL*
        ( IDENTIFIER /* symbols to export */ NL )*
    SYM_SCOPE_CLOSE
;

importDeclaration :
    'import' SYM_SCOPE_OPEN NL*
        ( fullyQualifiedModuleName NL? )*
    SYM_SCOPE_CLOSE
;

recordDefinition :
    'record' IDENTIFIER
    ( ':' IDENTIFIER ( ',' IDENTIFIER )* )? // inheriting props
    ( SYM_SCOPE_OPEN NL*
        ( recordMemberDefinition NL* )*
    SYM_SCOPE_CLOSE )?                      // records without { } are allowed
;

recordMemberDefinition :
    IDENTIFIER /* name */ IDENTIFIER /* type */
;

functionDefinition :
    'fun' ( functionIdentifier )                             // declares the name
    ( '(' functionParameter (',' functionParameter )* ')' )? // params are optional
    ( '->' IDENTIFIER )?                                     // return type is optional
    blockOfCode
;

functionParameter :
    IDENTIFIER /* name */ IDENTIFIER? /* type (optional) */
;

statement :
    ( recordDefinition
    | functionDefinition
    | loop
    | expression // functionCall, arithmetic, string, etc.
    | variableDefinition
    | variableAssignment )
;

// TODO maybe promote to expression?
loop :
    'foreach' ( IDENTIFIER | '_' ) /* lexical binding */ 'in'
    expression                     /* iterable */
    blockOfCode
;

variableDefinition:
    key=( MUTABLE_VAR_DEF | IMMUTABLE_VAR_DEF ) IDENTIFIER // declares the name
    IDENTIFIER?                                            // type can be inferred
    ( '=' expression )?
;

MUTABLE_VAR_DEF : 'var' ;
IMMUTABLE_VAR_DEF : 'let' ;

variableAssignment :
    IDENTIFIER OPERATOR? '=' expression
;

expression :
    OPERATOR expression                         # PrefixExpression      // precedence P1: prefix
    | expression functionIdentifier expression  # InfixExpression       // precedence P2: infix
    | expression op='^'<assoc=right> expression # StdInfixExpression    // precedence P3: exponentiation
    | expression op=( '*' | '/' ) expression    # StdInfixExpression    // precedence P4: mult/div
    | expression op=( '+' | '-' ) expression    # StdInfixExpression    // precedence P5: add/sub
    | conditional                               # ConditionalE
    | recordMemberSelection                     # RecordMemberSelectionE
    | symbolReference                           # SymbolReferenceE
    | constructorInvocation                     # ConstructorInvocationE
    | stringLiteral                             # StringLiteralE
    | numberLiteral                             # NumberLiteralE
    | mapLiteral                                # MapLiteralE
    | listLiteral                               # ListLiteralE
    | blockOfCode /* scoped code */             # BlockOfCodeE
;

conditional :
    'if' ifCondition=expression ifBranchBody=blockOfCode
    ( 'else if' elseIfCondition=expression elseIfBranchBody=blockOfCode )* // else-if is optional
    ( 'else' elseBranchBody=blockOfCode )?                                 // else is optional
;

recordMemberSelection :
    /* vvvvv should actually be an expression to allow for "wat".reverse etc. */
    IDENTIFIER '.' ( IDENTIFIER | recordMemberSelection )
    // for any `fun f(x, y)`, x.f(y) is also a member selection
    ( '(' expression ( ',' expression )* /* arguments */ ')' )?
;

symbolReference :
    IDENTIFIER ( '(' NL* expression NL* ( ',' NL* expression NL* )* /* arguments */ ')' )?
;

constructorInvocation :
    recordIdentifier=IDENTIFIER SYM_SCOPE_OPEN NL+
        ( fieldIdentifier=IDENTIFIER '=' expression NL+ )* // 0 or more initializations
    SYM_SCOPE_CLOSE
;

blockOfCode :
    SYM_SCOPE_OPEN NL*
        ( statement NL+ )*
    SYM_SCOPE_CLOSE
;

// TODO
mapLiteral :
    ('(:)' /* empty map */ )
;

// TODO list comprehensions?
listLiteral :
    '[' NL* ( expression NL* ( ',' NL* expression )* )? NL* ']'
;

// TODO optimize
//  -> move number recognition to lexer
numberLiteral :
    ( '0b' DIGIT+ )                                 #BinaryNumberLiteral
    | ( '0x' DIGIT+ )                               #HexNumberLiteral
    | ( '0o' DIGIT+ )                               #OctNumberLiteral
    | ( DIGIT+ )                                    #DecimalInteger
    | ( beforeComma=DIGIT* '.' afterComma=DIGIT+ )  #DecimalFloatingPoint
;

DIGIT : [0-9] ;

stringLiteral :
   STRING
;

STRING : '"' ( ESC | . )+? '"' ;
ESC : '\\' [btnr"\\] ; // \", \\, \b, \n, \t, \r

// allow nested modules
fullyQualifiedModuleName :
    IDENTIFIER ( '.' IDENTIFIER )*
;

functionIdentifier :
    OPERATOR       # OperatorFunctionIdentifier
    | IDENTIFIER   # AlphanumericFunctionIdentifier
;

IDENTIFIER : [a-zA-Z] [a-zA-Z0-9_]* ;

OPERATOR : [*+-/@'=&%^$£!;?<>`\\]+ ;

//WS : [ \t\r\n]+ -> skip ;
// ignore spaces and tabs
WS : [ \t]+ -> skip ;
// keep NL as statement separators
NL : '\r'? '\n' ;

SYM_SCOPE_OPEN : '{' ;
SYM_SCOPE_CLOSE : '}' ;

// line comments start w/ #
LINE_COMMENT : '#' ~[\r\n]* -> skip ;

// multi line comments are enclosed in #{  }#
MULTILINE_COMMENT : '#{' .*? '}#' -> skip ;