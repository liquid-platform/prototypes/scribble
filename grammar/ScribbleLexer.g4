lexer grammar ScribbleLexer;

DIGIT : [0-9] ;

STRING_START : '"' -> mode(STRING);

VARIABLE_IDENTIFIER : LOWERCASE_LETTER ( LOWERCASE_LETTER | UPPERCASE_LETTER | DIGIT | '_' )* ;

TYPE_IDENTIFIER : UPPERCASE_LETTER ( LOWERCASE_LETTER | UPPERCASE_LETTER | DIGIT | '_' )* ;

LOWERCASE_LETTER : [a-z] ;
UPPERCASE_LETTER : [A-Z] ;
OPERATOR : [*+-/@'=&%^$£!;?<>`\\]+ ;

//WS : [ \t\r\n]+ -> skip ;
// ignore spaces and tabs
WS : [ \t]+ -> skip ;
// keep NL as statement separators
NL : '\r'? '\n' ;

SYM_SCOPE_OPEN : '{' ;
SYM_SCOPE_CLOSE : '}' ;

// line comments start w/ #
LINE_COMMENT : '#' ~[\r\n]* -> skip ;

// multi line comments are enclosed in #{  }#
MULTILINE_COMMENT : '#{' .*? '}#' -> skip ;

mode STRING;
//STRING : '"' (ESC|.)*? (SYM_SCOPE_OPEN (ESC|.)*? SYM_SCOPE_CLOSE)? (ESC|.)*? '"' ;
//REGULAR_STRING_COMPONENT : {insideString}? (ESC | ~[{"] )+? ;
REGULAR_STRING_COMPONENT : ~[{"]+ ;
STRING_INTERPOLATION : SYM_SCOPE_OPEN ~'}'*? SYM_SCOPE_CLOSE;
ESC : '\\' [btnr"\\] ; // \", \\, \b, \n, \t, \r
STRING_END : '"' -> mode(DEFAULT_MODE);