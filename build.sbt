import scala.sys.process._

scalaVersion := "2.13.3"
// scalaVersion := "0.22.0-RC1"
name := "scribble-compiler"
version := "0.1.0"

libraryDependencies ++= Seq(
  "org.antlr" % "antlr4" % "4.8",
  "org.antlr" % "antlr4-runtime" % "4.8",
  "org.antlr" % "ST4" % "4.2",
  "org.typelevel" %% "cats-core" % "2.0.0",
  "org.scalactic" %% "scalactic" % "3.1.0",
  "org.scalatest" %% "scalatest" % "3.1.0" % "test",
  "org.typelevel" %% "spire" % "0.17.0-M1",
  "com.lihaoyi" %% "fansi" % "0.2.7",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "ch.qos.logback" % "logback-classic" % "1.2.3" // logging implementation
)

val antlrOutputDirectory =
  settingKey[File]("Directory for files generated by ANTLR")
antlrOutputDirectory := (Compile / javaSource).value / "antlr4"

val scribbleGrammarPath =
  settingKey[File]("The path of the Rain grammar file")
scribbleGrammarPath := baseDirectory.value / "grammar" / "Scribble"

lazy val antlr = taskKey[Unit]("Generates a parser from ANTLR grammar")
antlr := {
  val outDir = antlrOutputDirectory.value.absolutePath
  val grammar = scribbleGrammarPath.value.absolutePath

  val antlr4 = "java org.antlr.v4.Tool"

  val gen = s"$antlr4 -package antlr4 $grammar.g4 -o $outDir"
  val delTmpFiles = s"find $outDir -type f ! -name *.java -delete"

  (gen #&& delTmpFiles).run()
}

(compile in Compile) := ((compile in Compile) dependsOn antlr).value